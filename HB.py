#! /usr/bin/env python3

from datetime import datetime
import re
from math import trunc

import util
import mapUtil
from scenario import Story, StoryNavBar, DATA_JP
from reward import parseReward
from globals import DATA, DIFFICULTIES, ERROR, UNIT_IMAGE, UNITS

HP_STATS_MODIFIERS = [1.2, 1.3, 1.4, 1.5, 1.65]
def getStats(unitId: str, rarity: int=5, level: int=40):
    unit = UNITS[unitId]
    stats = {}
    statsOrder = list(unit['base_stats'].keys())
    statsOrder.sort(key=lambda k: unit['base_stats'][k], reverse=True)
    for key in unit['base_stats']:
        stats[key] = unit['base_stats'][key] - 1 + trunc((level - 1) * trunc(unit['growth_rates'][key] * (0.79+0.07*rarity)) / 100)
    for i in range(1, rarity):
        if i % 2 == 1:
            stats[statsOrder[1]] += 1
            stats[statsOrder[2]] += 1
        else:
            stats[statsOrder[0]] += 1
            stats[statsOrder[3]] += 1
            stats[statsOrder[4]] += 1
    return stats

def getHBHeroes(mapId: str):
    scenario = util.readFehData("USEN/Message/Scenario/"+mapId+".json")
    faceNames = list(dict.fromkeys(re.findall(r"ch\d{2}_\d{2}_\w+", scenario[1]['value'])))
    heroes = [UNIT_IMAGE[faceName] for faceName in faceNames]

    mapName = util.getName(mapId)
    heroesName = [util.getName(heroes[i]['id_tag']) for i in range(len(heroes))]
    if len(heroes) == 1:
        return heroes
    if mapName[2] == '&' and mapName[0] != mapName[4]:
        if heroesName[0][0] == mapName[0] and heroesName[1][0] == mapName[4]:
            return heroes
        elif heroesName[0][0] == mapName[4] and heroesName[1][0] == mapName[0]:
            return heroes[::-1]
    if DATA_JP['M'+heroes[1]['id_tag']] == DATA_JP['MID_STAGE_'+mapId]:
        return heroes
    elif DATA_JP['M'+heroes[0]['id_tag']] == DATA_JP['MID_STAGE_'+mapId]:
        return heroes[::-1]
    else:
        print(ERROR + f"Unexpected heroes on {mapName} ({mapId}): {', '.join(heroesName)}")

def HBMapInfobox(StageEvent: dict, group: str):
    info = {
        'id_tag': StageEvent['id_tag'],
        'banner': 'Banner ' + StageEvent['banner_id'] + '.webp',
        'group': group,
        'requirement': [],
        'lvl': {}, 'rarity': {}, 'stam': {}, 'reward': {},
        'map': {'id': StageEvent['id_tag'], 'player_pos': []},
        'bgms': util.getBgm(StageEvent['id_tag'])
    }
    if StageEvent['scenarios'][-1]['survives']:             info['requirement'] += ['All allies must survive.']
    if StageEvent['scenarios'][-1]['no_lights_blessing']:   info['requirement'] += ["Cannot use {{It|Light's Blessing}}."]
    if StageEvent['scenarios'][-1]['turns_to_win'] != 0:    info['requirement'] += [f"Turns to win: {StageEvent['scenarios'][-1]['turns_to_win']}"]
    if StageEvent['scenarios'][-1]['turns_to_defend'] != 0: info['requirement'] += [f"Turns to defend: {StageEvent['scenarios'][-1]['turns_to_defend']}"]
    info['requirement'] = '<br>'.join(info['requirement'])

    if StageEvent['scenarios'][-1]['reinforcements']:
        info['mode'] = 'Reinforcement Map'
    for index in range(len(StageEvent['scenarios'])):
        diff = DIFFICULTIES[StageEvent['scenarios'][index]['difficulty']]
        info['lvl'].update({diff: StageEvent['scenarios'][index]['true_lv']})
        info['rarity'].update({diff: StageEvent['scenarios'][index]['stars']})
        info['stam'].update({diff: StageEvent['scenarios'][index]['stamina']})
        info['reward'].update({diff: StageEvent['scenarios'][index]['reward']})

    return mapUtil.MapInfobox(info)

def HBUnitData(StageEvent: dict, heroes):
    s = "==Unit data==\n"
    s += "{{#invoke:UnitData|main|globalai="
    if StageEvent['scenarios'][-1]['reinforcements']:
        s += "\n|mapImage=" + mapUtil.MapImage({'id': StageEvent['id_tag']}, True, True)
    for scenario in StageEvent['scenarios']:
        s += '\n|' + DIFFICULTIES[scenario['difficulty']] + '='
        units = []
        for w in scenario['enemy_weps']:
            if w == -1: continue
            units += [{'rarity': scenario['stars'], 'true_lv': scenario['true_lv']}]
            if scenario['difficulty'] > 2: units[-1]['refine'] = True
        for i, h in enumerate(heroes):
            units[i]['id_tag'] = h['id_tag']
            units[i]['cooldown_count'] = None
            units[i]['stats'] = getStats(h['id_tag'], scenario['stars'], scenario['true_lv'])
            if scenario['difficulty'] >= 4:
                for k in units[i]['stats']: units[i]['stats'][k] += 4
            units[i]['stats']['hp'] = int(units[i]['stats']['hp'] * HP_STATS_MODIFIERS[scenario['difficulty']])
        if scenario['reinforcements']:
            units += [{'rarity': scenario['stars'], 'true_lv': scenario['true_lv'], 'spawn_count': 0}]
            if scenario['difficulty'] > 2: units[-1]['refine'] = True
        s += mapUtil.UnitData({'units': units})
    return s + "\n}}\n"

# def LegendaryHeroBattle(mapId: str):
#     StageEvent = util.fetchFehData("Common/SRPG/StageEvent")[mapId]

#     heroes = getHBHeroes(mapId)
#     if len(heroes) > 1 and heroes[0]['legendary']['element'] != heroes[1]['legendary']['element']:
#         kind = 'Legendary & Mythic Hero Battle'
#     else:
#         kind = heroes[0]['legendary']['element'] > 4 and 'Mythic Hero Battle' or 'Legendary Hero Battle'

#     content =  HBMapInfobox(StageEvent, kind) + "\n"
#     content += mapUtil.MapAvailability(StageEvent['avail'], f"{kind}! ({datetime.strptime(StageEvent['avail']['start'], util.TIME_FORMAT).strftime('%b %Y')}) (Notification)", f"[[{kind}]]")
#     content += HBUnitData(StageEvent, heroes) + '\n'
#     content += "==Story==\n" + Story(mapId) + '\n' + StoryNavBar(mapId) + "\n"
#     content += "==Trivia==\n*\n"
#     content += mapUtil.InOtherLanguage(["MID_STAGE_"+mapId, "MID_STAGE_HONOR_"+mapId], 1)
#     content += "{{Special Maps Navbox}}"
#     return content

# def BoundHeroBattle(mapId: str):
#     StageEvent = util.fetchFehData("Common/SRPG/StageEvent")[mapId]

#     heroes = getHBHeroes(mapId)
#     name = util.getName(mapId)
#     if name[0] != name[4]:
#         name = name.replace(name[4], DATA["M"+heroes[1]['id_tag']], 1).replace(name[0], DATA["M"+heroes[0]['id_tag']], 1)
#     else:
#         name = name.replace(name[0], DATA["M"+heroes[1]['id_tag']], 2).replace(DATA["M"+heroes[1]['id_tag']], DATA["M"+heroes[0]['id_tag']], 1)

#     content =  HBMapInfobox(StageEvent, "Bound Hero Battle") + "\n"
#     content += mapUtil.MapAvailability(StageEvent['avail'], f"Bound Hero Battle: {DATA['M'+heroes[0]['id_tag']]} & {DATA['M'+heroes[1]['id_tag']]} (Notification)", "[[Bound Hero Battle]]")
#     content += HBUnitData(StageEvent, heroes) + '\n'
#     content += "==Story==\n" + Story(mapId) + '\n' + StoryNavBar(mapId) + "\n"
#     content += "==Trivia==\n*\n"
#     content += mapUtil.InOtherLanguage(["MID_STAGE_"+mapId, "MID_STAGE_HONOR_"+mapId], 1)
#     content += "{{Special Maps Navbox}}"

#     return {name: content}

# def GrandHeroBattle(mapId: str):
#     StageEvent = util.fetchFehData("Common/SRPG/StageEvent")[mapId]
#     hero = getHBHeroes(mapId)

#     content =  HBMapInfobox(StageEvent, "Grand Hero Battle") + "\n"
#     content += mapUtil.MapAvailability(StageEvent['avail'], f"Grand Hero Battle - {util.getName(mapId)} (Notification)", "[[Grand Hero Battle]]")
#     content += HBUnitData(StageEvent, hero) + '\n'
#     content += "==Story==\n" + Story(mapId) + '\n' + StoryNavBar(mapId) + "\n"
#     content += "==Trivia==\n*\n"
#     content += mapUtil.InOtherLanguage(["MID_STAGE_"+mapId, "MID_STAGE_HONOR_"+mapId], 1)
#     content += "{{Special Maps Navbox}}"

#     return content

def HeroBattle(mapId: str):
    StageEvent = util.fetchFehData("Common/SRPG/StageEvent")[mapId]

    heroes = getHBHeroes(mapId)
    if len(heroes) == 1:
        name = util.getName(mapId) + ' (map)'
        kind = 'Grand Hero Battle' if not heroes[0]['legendary'] else 'Legendary Hero Battle' if heroes[0]['legendary']['element'] < 4 else 'Mythic Hero Battle'
    else:
        name = DATA['M'+heroes[0]['id_tag']] + DATA['MID_STAGE_'+mapId][1:-1] + DATA['M'+heroes[1]['id_tag']] + ': ' + DATA['MID_STAGE_HONOR_'+mapId]
        kind = list({'Legendary' if h['legendary']['element'] <= 4 else 'Mythic' for h in heroes if h['legendary'] and h['legendary']['element'] in range(1,9)})
        kind = 'Bound Hero Battle' if len(kind) == 0 else 'Legendary & Mythic Hero Battle' if len(kind) == 2 else (kind[0] + ' Hero Battle')
    if kind == 'Bound Hero Battle':
        notif = f"Bound Hero Battle: {name[:name.find(':')]} (Notification)"
    elif kind == 'Grand Hero Battle':
        notif = f"Grand Hero Battle - {util.getName(mapId)} (Notification)"
    else:
        notif = f"{kind}! ({datetime.strptime(StageEvent['avail']['start'], util.TIME_FORMAT).strftime('%b %Y')}) (Notification)"

    content =  HBMapInfobox(StageEvent, kind) + "\n"
    content += mapUtil.MapAvailability(StageEvent['avail'], notif, f"[[{kind}]]")
    content += HBUnitData(StageEvent, heroes) + '\n'
    content += "==Story==\n" + Story(mapId) + '\n' + StoryNavBar(mapId) + "\n"
    content += "==Trivia==\n*\n"
    content += mapUtil.InOtherLanguage(["MID_STAGE_"+mapId, "MID_STAGE_HONOR_"+mapId], name)
    content += "{{Special Maps Navbox}}"
    return {name: content}


def LimitedHeroBattleTemplate(StageEvent: object):
    entry = []
    obj = {DIFFICULTIES[scenario['difficulty']]: scenario['reward'] for scenario in StageEvent['scenarios']}
    reward = "{\n"
    for diff in DIFFICULTIES:
        if diff in obj:
            reward += "  " + diff + "=" + parseReward(obj[diff]) + ";\n"
    reward += "}"
    if isinstance(StageEvent['scenarios'][0]['origins'], str):
        StageEvent['scenarios'][0]['origins'] = int(StageEvent['scenarios'][0]['origins'], base=0)
    for i in range(8*4):
        if StageEvent['scenarios'][0]['origins'] & (1 << i):
            entry.append(str(i))
    return "{{Limited Hero Battle\n" + \
        f"|map={StageEvent['id_tag']}|entry={','.join(entry)}" +\
        f"|refresher={StageEvent['scenarios'][0]['max_refreshers']}\n" + \
        f"|reward={reward}\n" + \
        f"|start={StageEvent['avail']['start']}|end={util.timeDiff(StageEvent['avail']['finish'])}\n" + \
        f"|notification=Limited Hero Battles! ({datetime.strptime(StageEvent['avail']['start'], util.TIME_FORMAT).strftime('%b %Y')}) (Notification)\n" + \
        "}}"

def LimitedHeroBattle(mapId: str):
    from wikiUtil import getPageContent
    StageEvent = util.fetchFehData("Common/SRPG/StageEvent")[mapId]
    pageName = util.cargoQuery('Maps', where=f"Map='{StageEvent['banner_id']}'", limit=1).replace('&amp;', '&')
    content = getPageContent([pageName])[pageName]

    if not re.search(r"==\s*Limited Hero Battle\s*==", content):
        content = re.sub(r"(==\s*Unit [dD]ata\s*==(\n.*)*?)\n==", "\\1\n==Limited Hero Battle==\n==", content)
    if content.find("{{Limited Hero Battle/header}}") == -1:
        content = re.sub(r"(==\s*Limited Hero Battle\s*==)\n", "\\1\n{{Limited Hero Battle/header}}\n|}\n", content)
    if not re.search(r"map\s*=\s*"+mapId, content):
        content = re.sub(r"(==\s*Limited Hero Battle\s*==(\n.*)*?)\n\|\}", "\\1\n"+LimitedHeroBattleTemplate(StageEvent)+"\n|}", content)
    return {pageName: content}

def RevivalHeroBattle(mapId: str):
    from wikiUtil import getPageContent
    StageEvent = util.fetchFehData("Common/SRPG/StageEvent")[mapId]
    pageName = util.cargoQuery('Maps', where=f"Map='{StageEvent['banner_id']}'", limit=1).replace('&amp;', '&')
    content = getPageContent([pageName])[pageName]

    if mapId[0] in ['I', 'Q', 'V'] or re.search(r"start\s*=\s*"+StageEvent['avail']['start']+r"\s*\|end\s*=\s*"+util.timeDiff(StageEvent['avail']['finish']), content):
        return {}
    
    starttime = datetime.strptime(StageEvent['avail']['start'], util.TIME_FORMAT)
    if starttime >= datetime.strptime(util.timeDiff(StageEvent['avail']['finish'], 86400*4), util.TIME_FORMAT):
        notification = ""
    else:
        kind = re.search(r"mapGroup\s*=\s*(.*)\n", content)[1]
        
        if kind.find('Legendary') != -1 or kind.find('Mythic') != -1:
            year = int(StageEvent['avail']['start'][:4])
            month = int(StageEvent['avail']['start'][5:7])
            day = int(StageEvent['avail']['start'][8:10])
            if day > 22 or day < 3:
                if day < 3: month -= 1
                if month % 3 != 1:
                    notification = f"Legendary Hero Battle! ({datetime(year=year,month=month,day=1).strftime('%b %Y')}) (Notification)"
                else:
                    notification = f"Mythic Hero Battle! ({datetime(year=year,month=month,day=1).strftime('%b %Y')}) (Notification)"
            else:
                notification = f'Legendary & Mythic Hero Remix ({datetime(year=year,month=month,day=1).strftime("%b %Y")}) (Notification)'
    
        elif kind.find('Bound') != -1:
            if content.find('=Bound Hero Battle Revival') == -1:
                notification = f"Bound Hero Battle Revival: {pageName[:pageName.find(':')]} (Notification)"
            else:
                notification = f"Bound Hero Battle Revival: {pageName[:pageName.find(':')]} ({starttime.strftime('%b %Y')}) (Notification)"

        elif kind.find('Grand') != -1:
            if content.find('=Grand Hero Battle Revival') == -1:
                notification = f"Grand Hero Battle Revival - {pageName[:pageName.find(' (')]} (Notification)"
            else:
                notification = f"Grand Hero Battle Revival - {pageName[:pageName.find(' (')]} ({starttime.strftime('%b %Y')}) (Notification)"

        else:
            print(util.TODO + "Unknow revival")
            return {}
    
    content = re.sub(r"(\{\{MapDates[^\n]*)(\s*?\n)*(==\s*Unit [Dd]ata\s*==)", f"\\1\n* {{{{MapDates|start={StageEvent['avail']['start']}|end={util.timeDiff(StageEvent['avail']['finish'])}|notification={notification}}}}}\n\\3", content)
    return {pageName: content}


from sys import argv

if __name__ == '__main__':
    if len(argv) > 1 and re.match(r'[PE]ID_\w+', argv[1]):
        lvl = int(argv[2]) if len(argv) > 2 and re.match('^\d+$',argv[2]) else 40
        print(getStats(argv[1], 5 if lvl > 30 else 4 if lvl > 15 else 3, lvl))
        exit(0)
    for i,arg in enumerate(argv[1:]):
        if re.match(r'[TL]\d{4}', arg):
            print(HeroBattle(arg))
        elif re.match(r'I\d{4}', arg):
            print(LimitedHeroBattle(arg))
        else:
            print(util.ERROR, "Unknow argument", arg)