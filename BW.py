#! /usr/bin/env python3

from os.path import isfile

import util
from Reverse import reverseBindingWorlds
from reward import parseReward

def BWInfobox(data: dict, nb: int):
    return "{{Binding Worlds Infobox\n" + \
        f"|number={nb}\n" + \
        f"|startTime={data['avail']['start']}\n" + \
        f"|endTime={util.timeDiff(data['avail']['finish'])}\n}}}}"

def BWRewards(data: dict):
    s = "==Rewards==\n===Daily rewards===\n{{#invoke:Reward/BindingWorlds|daily\n"
    for i in range(len(data['daily_rewards'])):
        s += f" |{i+1}=" + parseReward(data['daily_rewards'][i]['reward']) + "\n"
    s += "}}\n===Enclosure cleared===\n{{#invoke:Reward/BindingWorlds|stages\n"
    for chamber in data['enclosures']:
        s += f" |{chamber['id_tag']:>2}=" + parseReward(chamber['reward']) + "\n"
    return s + "}}"

def BindingWorlds(tag: str):
    datas = reverseBindingWorlds(tag)

    ret = {}
    for data in datas:
        nb = int(util.cargoQuery('BindingWorlds', 'COUNT(DISTINCT _pageName)=Nb', where=f"StartTime < '{data['avail']['start']}'", limit=1)) + 1
        s = BWInfobox(data, nb) + "\n"
        s += BWRewards(data) + "\n"
        s += "{{Main Events Navbox}}"
        ret[f"Binding Worlds {nb}"] = s
    return ret

from sys import argv

if __name__ == '__main__':
    if len(argv) <= 1:
        print("Enter at least one update tag")
        exit(1)
    for arg in argv[1:]:
        if not isfile(util.BINLZ_ASSETS_DIR_PATH + 'Common/SRPG/ConnectBonds/' + arg + '.bin.lz'):
            print(f'No Binding Worlds are related to the tag "{arg}"')
            continue
        BWs = BindingWorlds(arg)
        for BW in BWs:
            print(BW, BWs[BW])