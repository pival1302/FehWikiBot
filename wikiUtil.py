#!/usr/bin/env python3

from math import ceil
import requests
from time import sleep
from datetime import datetime, timedelta
from io import BytesIO

import util
from util import cargoQuery


now = 0
def waitSec(time: int):
    global now
    if now != 0 and datetime.now() < now+timedelta(seconds=time):
        sleep((now+timedelta(seconds=time)-datetime.now()).total_seconds())
    now = datetime.now()

MAXIMUM_RETRY = 3
def request_get(params):
    S = util.fehBotLogin()
    params["format"] = "json"
    for i in range(MAXIMUM_RETRY):
        try:
            result = S.get(url=util.URL, params=params).json()
            return result
        except (requests.exceptions.Timeout, requests.exceptions.ConnectionError) as e:
            if i == MAXIMUM_RETRY-1: raise e

def getPageRevision(page: str, revision: int) -> str:
    result = request_get({
        "action": "query",
        "titles": page,
        "prop": "revisions",
        "rvprop": "content",
        "rvlimit": revision+1,
        "rvslots": "*",
    })
    result = list(result['query']['pages'].values())[0]['revisions']
    if len(result) > revision:
        return result[revision]['slots']['main']['*']
    else:
        return result[-1]['slots']['main']['*']

def getPageContent(pages: list) -> dict:
    from types import GeneratorType as generator
    if isinstance(pages, str): pages = [pages]
    elif isinstance(pages, generator): pages = list(pages)
    contents = {}
    for i in range(ceil(len(pages) / 50)):
        result = request_get({
            "action": "query",
            "titles": "|".join(pages[i*50:(i+1)*50]),
            "prop": "revisions",
            "rvprop": "content",
            "rvslots": "*",
        })
        result = result['query']['pages']
        result = {result[pageId]['title']: result[pageId]['revisions'][0]['slots']['main']['*'] for pageId in result if 'revisions' in result[pageId]}
        contents.update(result)
    return contents

def getPagesOnCategory(cat):
    return [o['title'] for o in request_get({
        "action": "query",
        "list": "categorymembers",
        "cmtitle": ('Category:' if cat[:9] != 'Category:' else '') + cat,
        "cmlimit": "max",
        "format": "json",
    })['query']['categorymembers']]

def getPagesLinkingTo(pages):
    if isinstance(pages, list): pages = '|'.join(pages)
    elif isinstance(pages, dict): pages = '|'.join(pages.keys())
    result = request_get({
        "action": "query",
        "prop": "linkshere|transcludedin",
        "titles": pages,
        "lhprop": "title",
        "lhlimit": "max",
        "tiprop": "title",
        "tilimit": "max",
        "format": "json",
    })
    result = {
        o['title']: ([c['title'] for c in o['linkshere']] if 'linkshere' in o else []) + ([c['title'] for c in o['transcludedin']] if 'transcludedin' in o else [])
    for o in result['query']['pages'].values()}
    return result if len(result) > 1 else result[pages] 

def getCategoriesOf(pages):
    if isinstance(pages, list): pages = '|'.join(pages)
    elif isinstance(pages, dict): pages = '|'.join(pages.keys())
    result = request_get({
        "action": 'query',
        'prop': 'categories',
        'titles': pages,
        'cllimit': 'max',
        'format': 'json'
    })
    result = {o['title']: ([c['title'] for c in o['categories']] if 'categories' in o else []) for o in result['query']['pages'].values()}
    return result if len(result) > 1 else result[pages]

def request_post(params, **kwargs):
    S = util.fehBotLogin()
    params.update({
        "bot": True,
        "tags": "automated",
        "watchlist": "nochange",
        "token": util.getToken(),
        "format": "json"
    })
    for i in range(MAXIMUM_RETRY):
        result = None
        try:
            result = S.post(url=util.URL, data=params, timeout=10, **kwargs)
            result = result.json()
            return result
        except requests.exceptions.ReadTimeout:
            return {'error': {'info': 'Timeout'}}
        except (requests.exceptions.ConnectTimeout, requests.exceptions.ConnectionError) as e:
            waitSec(10)
            if i == MAXIMUM_RETRY-1: raise e
        except:
            return {'error':{'info':str(result)}}

def _exportPage(name: str, content: str, summary: str=None, minor: bool=False, create: bool=False):
    return request_post({
        "action": "edit",
        "title": name,
        "text": content,
        "summary": summary,
        ("minor" if minor else "major"): True,
        ("" if create == -1 else "createonly" if create else "nocreate"): True
    })

def exportPage(name: str, content: str, summary: str=None, minor: bool=False, create: bool=False):
    result = _exportPage(name, content, summary, minor, create)
    if 'edit' in result and result['edit']['result'] == 'Success':
        if 'nochange' in result['edit']:
            print(f"No change: " + name)
        elif 'new' in result['edit']:
            print(f"Page created: " + name)
        else:
            print(f"Page edited: " + name)
    elif 'error' in result and 'code' in result['error'] and result['error']['code'] == 'articleexists':
        print(f"Page already exist: " + name)
    elif 'error' in result and 'info' in result['error']:
        print(util.ERROR + f"Error on \"{name}\": {result['error']['info']}")
    else:
        print(util.ERROR + str(result))

def exportSeveralPages(group: dict, summary: str=None, minor: bool=False, create: bool=False):
    for name in group:
        waitSec(10)
        exportPage(name, group[name], summary, minor, create)

def uploadFile(name: str, file, content: str, comment: str, ignoreWarning: bool=True):
    result = request_post({
        "action": "upload",
        "filename": util.cleanStr(name),
        "comment": comment,
        "text": content,
        ("ignorewarnings" if ignoreWarning else "warnings"): True,
    }, files={
        'file': (name, file, 'multipart/form-data')
    })
    if 'error' in result and 'code' in result['error'] and result['error']['code'] == 'mustbeloggedin':
        util.SESSION = None
        uploadFile(name, file, content, comment, ignoreWarning)
    elif 'error' in result and 'code' in result['error'] and result['error']['code'] == 'fileexists-no-change':
        print(f"File already exist: {name}")
    elif 'upload' in result and result['upload']['result'] == 'Success':
        print(f"File uploaded: {name}")
    elif 'error' in result and 'info' in result['error']:
        print(util.ERROR + f"Error with file \"{name}\": {result['error']['info']}")
    else:
        print(util.ERROR + result)

def uploadImage(name: str, image, content: str, comment: str, ignoreWarning: bool):
    file = BytesIO()
    image.save(file, format='PNG')
    uploadFile(name, file.getvalue(), content, comment, ignoreWarning)

def uploadFileUrl(name: str, fileUrl: str, content: str, comment: str, ignoreWarning: bool):
    for i in range(MAXIMUM_RETRY):
        try:
            data = requests.get(fileUrl).content; break
        except(requests.exceptions.Timeout, requests.exceptions.ConnectTimeout, requests.exceptions.ConnectionError):
            if i == MAXIMUM_RETRY-1:
                print(util.ERROR + f'Failed to get file "{name}"'); return
    uploadFile(name, data, content, comment, ignoreWarning)

def movePage(name: str, newName: str, summary: str=None, redirect: bool=True):
    result = request_post({
        "action": "move",
        "from": name,
        "to": newName,
        "movetalk": True,
        "movesubpages": True,
        ("redirect" if redirect else "noredirect"): True,
        "reason": summary,
    })
    if 'move' in result:
        print('Page moved: ' + name)
    elif 'error' in result and 'info' in result['error']:
        print(util.ERROR + f"Failed to move \"{name}\": {result['error']['info']}")
    else:
        print(util.ERROR + f'{result}')

def deletePage(name: str, summary: str=None):
    result = request_post({
        "action": "delete",
        "title": name,
        "reason": summary,
    })
    if 'delete' in result:
        print('Page deleted: ' + name)
    elif 'error' in result and 'info' in result['error']:
        print(util.ERROR + f"Failed to delete \"{name}\": {result['error']['info']}")
    else:
        print(util.ERROR + f'{result}')

def deleteToRedirect(pageToDelete: str, redirectionTarget: str):
    S = util.fehBotLogin()
    deleteR = request_post({
        "action": "delete",
        "title": pageToDelete,
        "reason": 'Bot: Delete to redirect',
    })
    if 'error' in deleteR:
        print(util.ERROR + f'Failed to delete page {pageToDelete}: {deleteR["error"]["info"]}')
    elif 'delete' in deleteR:
        redirectR = _exportPage(pageToDelete, f"#REDIRECT [[{redirectionTarget}]]", "Bot: redirect", create=True)
        if 'error' in redirectR:
            print(util.ERROR + f'Failed to redirect page {pageToDelete} to {redirectionTarget}: {redirectR["error"]["info"]}')
        else:
            print(f'Redirected page {pageToDelete} to {redirectionTarget}')

if __name__ == '__main__':
    pass