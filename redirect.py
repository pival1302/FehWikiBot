#! /usr/bin/env python3

from sys import argv
from datetime import datetime
from os.path import exists
from PIL import Image
import json
import re

import util
from globals import TODO, DATA, SKILLS, ACCESSORIES
from wikiUtil import _exportPage, request_get, uploadImage

def getWeaponName(sprite: str):
    nameId = None
    for skill in SKILLS.values():
        if nameId:
            break
        for s in skill['sprites']:
            if s and s == sprite[:-5].lower().replace(' ', '_'):
                nameId = skill['name_id']
    if nameId and nameId in DATA:
        return "File:Weapon " + util.cleanStr(DATA[nameId]) + (" V2" if sprite.find("ar") != -1 else "") + ".png"

def getAccessoryName(sprite: str):
    if sprite in ACCESSORIES:
        return f"File:Accessory {util.cleanStr(util.getName(ACCESSORIES[sprite]['id_tag']))}.png"

def redirect(name: str, redirect: str):
    result = _exportPage(redirect or name.replace(".webp", ".png"),
                        "#REDIRECT [[" + name + "]]", create=True)
    if 'error' in result and result['error']['code'] == 'articleexists':
        print(f"Redirect already exist: {redirect or name.replace('.webp', '.png')}")
    elif 'edit' in result and result['edit']['result'] == 'Success':
        print(f"Redirect {name} to {redirect or name.replace('.webp', '.png')}")
    else:
        print(json.dumps(result, indent=2))

def handleWepMg(name: str, wep: str):
    path = util.WEBP_ASSETS_DIR_PATH + 'Common/Wep/' + name.lower().replace('.webp','')
    img = Image.open(path + '.png')
    if exists(path + '.ssbp') or img.size == (8,8):
        if name.find("_up.") == -1:
            redirect('File:'+name, wep)
        else:
            print(TODO + f"Refine wep: {name} ({wep})")
    elif exists(path + '.png'):
        wep = wep.replace('File:','')
        if name.find('_up.') == -1:
            uploadImage(wep,                           img.crop((0, 0, 56, 64)), '', 'Bot: Tome sprite', True)
            uploadImage(wep.replace('.png',' V2.png'), img.crop((48, 0, 128, 64)), '', 'Bot: Tome sprite', True)
        else:
            uploadImage(wep.replace('.png',' V3.png'), img.crop((0, 0, 56, 64)), '', 'Bot: Tome sprite', True)
            uploadImage(wep.replace('.png',' V4.png'), img.crop((48, 0, 128, 64)), '', 'Bot: Tome sprite', True)
    else:
        print(TODO + f"{name} to {wep}")

def main(start=None):
    result = request_get({
        "action": "query",
        "list": "allimages",
        "aisort": "timestamp",
        "aistart": start if start else datetime.utcnow().strftime('%Y-%m-%dT00:00:00Z'),
        "aiprop": "",
        "ailimit": "max",
    })

    for image in result['query']['allimages']:
        file = image['title']
        name = None
        if re.match(r"Map_[A-Z]\w\d{3}\.webp", image['name']):
            name = file.replace(".webp", ".png")
        elif re.match(r"TT_\d{6}(\s\d{2})?\.webp", image['name']):
            if 'MID_SEQUENTIAL_MAP_TERM_' + image['name'][3:-5] in DATA:
                name = "File:Banner " + util.cleanStr(DATA['MID_SEQUENTIAL_MAP_TERM_' + image['name'][3:-5]]) + ".png"
            else:
                print(TODO + "TT banner: " + image['title'])
        elif re.match(r"Wep_[a-z]{2}\d{3}(_up)?\.webp", image['name']):
            wp = getWeaponName(image['name'])
            if not wp:
                print(TODO + "Unknow weapon: " + image['title'])
            elif image['name'].find("mg") != -1:
                handleWepMg(image['name'], getWeaponName(image['name']))
            elif image['name'].find("up") != -1:
                print(TODO + f"Refine wep: {image['name']} ({wp})")
            else:
                name = wp
        elif re.match(r"Acc[_ ][1-4][_ ]\d{4}[_ ]\d\.webp", image['name']):
            name = getAccessoryName(image['name'][:-5])
            if not name:
                print(TODO + "Unknow accessory: " + image['title'])
        elif re.search(r"[_ ]Face[_ ]FC\.webp$", image['name']):
            name = file.replace(".webp", ".png")
        elif re.match(r"GC[_ ]\d{6}([_ ]\d{2})?\.webp", image['name']):
            print(TODO + "Grand conquest map: " + image['title'])
        elif re.match(r"Talk[_ ].+\.webp", image['name']):
            name = file.replace(".webp", ".png")
        elif re.match(r"EvBg[_ ].+\.webp", image['name']):
            name = "File:Talk " + image['name'].replace(".webp", ".png")
        elif re.match(r"Wallpattern[_ ].+\.webp", image['name']):
            name = file.replace(".webp", ".png")
        elif image['name'][-5:] == '.webp':
            print("Other webp file: " + image['title'])
        if name:
            redirect(file, name)


if __name__ == "__main__":
    main(argv[1] if len(argv) == 2 else None)
