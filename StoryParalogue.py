#! /usr/bin/env python3

import re
from os.path import exists
from datetime import datetime

import util
from globals import DATA, DIFFICULTIES, ROMAN, UNIT_NAMES
import mapUtil
import wikiUtil
from scenario import StoryNavBar, parseScenario, ScenarioNavbar, ScenarioNavbox

def StoryMapInfobox(stage: dict, field: dict, mapId: str, index: int):
    map = {}
    map['banner'] = stage['id_tag'] + (' C' if index == 4 else '')  + ".webp"
    map['id_tag'] = mapId
    if stage['maps'][0]['scenarios'][index]['reinforcements']:
        map['mode'] = 'Reinforcement Map'
    if stage['book']:
        map['book'] = 'Book ' + ROMAN[stage['book']]
    map['group'] = util.getName(stage['id_tag']) if stage['id_tag'][:3] != 'CXX' else 'Xenologue'
    map['map'] = field
    map['stam'] = {}
    map['lvl'] = {}
    map['rarity'] = {}
    map['reward'] = {}
    for idiff in range(len(stage['maps'])):
        diff = DIFFICULTIES[stage['maps'][idiff]['scenarios'][index]['difficulty']]
        map['lvl'].update({diff: stage['maps'][idiff]['scenarios'][index]['true_lv']})
        map['rarity'].update({diff: stage['maps'][idiff]['scenarios'][index]['stars']})
        map['stam'].update({diff: stage['maps'][idiff]['scenarios'][index]['stamina']})
        map['reward'].update({diff: stage['maps'][idiff]['scenarios'][index]['reward']})

    map['requirement'] = []
    if stage['maps'][0]['scenarios'][index]['survives']: map['requirement'] += ['All allies must survive.']
    if stage['maps'][0]['scenarios'][index]['no_lights_blessing']: map['requirement'] += ["Cannot use {{It|Light's Blessing}}."]
    if stage['maps'][0]['scenarios'][index]['turns_to_win'] != 0: map['requirement'] += [f"Turns to win: {stage['maps'][0]['scenarios'][index]['turns_to_win']}"]
    if stage['maps'][0]['scenarios'][index]['turns_to_defend'] != 0: map['requirement'] += [f"Turns to defend: {stage['maps'][0]['scenarios'][index]['turns_to_defend']}"]
    map['requirement'] = '<br>'.join(map['requirement'])
    map['bgms'] = util.getBgm(mapId)

    return mapUtil.MapInfobox(map)

def StoryUnitData(SRPGMap: list, StageEvent: dict, index: int):
    s = "==Unit data==\n{{#invoke:UnitData|main\n"

    if StageEvent['maps'][0]['scenarios'][index]['reinforcements']:
        s += "|mapImage=" + mapUtil.MapImage(SRPGMap[0]['field'], True, True) + "\n"

    for idiff in range(len(SRPGMap)):
        s += "|" + DIFFICULTIES[StageEvent['maps'][idiff]['scenarios'][index]['difficulty']] + "="
        s += mapUtil.UnitData(SRPGMap[idiff]) + "\n"

    return s + "}}\n"

def MapStory(mapId, categories=[]):
    if not exists(util.JSON_ASSETS_DIR_PATH + 'USEN/Message/Scenario/' + mapId + '.json'):
        return ''
    scenar = parseScenario(mapId)
    s = "{{#invoke:Scenario|story\n"
    if 'OPENING' in scenar:
        s += '|opening=' + scenar['OPENING'] + '\n'
    if 'MAP_BEGIN' in scenar:
        if mapId[0] == 'S' and scenar['MAP_BEGIN'].count('{unit=') == 1 and UNIT_NAMES[re.search(r'unit=([^;]+)', scenar['MAP_BEGIN'])[1]]['face_name'][:5] != 'ch00_':
            if 'OPENING' in scenar:
                s += '}}\n{{#ifeq:{{BASEPAGENAME}}|{{subst:BASEPAGENAME}}|{{#invoke:Scenario|story\n'
            else:
                s = '{{#ifeq:{{BASEPAGENAME}}|{{subst:BASEPAGENAME}}|' + s
            s += '|map begin=' + scenar['MAP_BEGIN'] + '}}'
            if 'MAP_END' in scenar or 'ENDING' in scenar:
                s += '}}\n{{#invoke:Scenario|story\n'
        else:
            s += '|map begin=' + scenar['MAP_BEGIN'] + '\n'
    for tag in ['MAP_END','ENDING']:
        if tag in scenar:
            s += '|' + tag.lower().replace('_',' ') + '=' + scenar[tag] + '\n'
    s += '}}'
    if len([tag for tag in scenar if tag not in ['OPENING','MAP_BEGIN','MAP_END','ENDING']]) > 0:
        print('Others tag present')
    if len(categories) > 0:
        s += '<noinclude>' + ''.join('[[Category:'+c+' scenarios]]' for c in categories) + '</noinclude>'
    return s

def GroupStory(object: dict):
    maps = {k:v for k,v in object.items() if k[-6:] != '/Story'}
    if len(maps) == 0: return
    category = 'story' if len(maps) == 5 else 'paralogue' if len(maps) == 3 else ''
    start = re.search(r'start=([^|\n}]*)',list(maps.values())[0])[1]
    story = ''
    for i,(title,_) in enumerate(sorted(maps.items(), key=lambda o: re.search(r'baseMap=(\w{5})',o[1])[1])):
        if (title+'/Story') not in object: continue
        if category != 'story' or object[title+'/Story'].find('{{#ifeq:') == -1 or re.search('opening=|map end=|ending=', object[title+'/Story']):
            story += f"==[[{title}|Part {i+1}]]==\n"+'{{:'+title+'/Story}}\n'
    if category == 'paralogue':
        TTs = {tt['avail']['start']:tt for tt in util.fetchFehDataFromAssets('Common/SRPG/SequentialMap',None)}
        if start in TTs:
            tt = util.getName('MID_SEQUENTIAL_MAP_TERM_' + TTs[start]['id_tag'])
            story += '==[['+tt+'|Extra]]==\n{{:'+tt+'/Story}}\n'
        elif util.timeDiff(start, -86400) in TTs:
            tt = util.getName('MID_SEQUENTIAL_MAP_TERM_' + TTs[util.timeDiff(start, -86400)]['id_tag'])
            story += '==[['+tt+'|Extra]]==\n{{:'+tt+'/Story}}\n'
    story += ScenarioNavbar(category, start, re.search(r'mapGroup=([^|\n}]*)',list(maps.values())[0])[1]) + '\n'
    story += ScenarioNavbox(category)
    return story

def StoryMap(mapId: str, StageScenario: dict=None, index: int=None, notif: str=''):
    if not StageScenario:
        for scenario in util.fetchFehData("Common/SRPG/StageScenario", None):
            for m in scenario['maps'][0]['scenarios']:
                if m['id_tag'][:-1] == mapId:
                    StageScenario = scenario
                    index = scenario['maps'][0]['scenarios'].index(m)
                    groupName = util.getName(scenario['id_tag'])
                    notif = util.askFor(None, f"What is the notification for {groupName} ({scenario['id_tag']})?") or ''

    SRPGMap = [util.readFehData("Common/SRPGMap/" + Id + ".json") for Id in [StageScenario['maps'][i]['scenarios'][index]["id_tag"] for i in range(3)]]
    title = util.getName(mapId, False)

    SRPGMap[0]['field'].update({'player_pos': SRPGMap[0]['player_pos']})

    content = StoryMapInfobox(StageScenario, SRPGMap[0]['field'], mapId, index) + '\n'
    content += mapUtil.MapAvailability(StageScenario['avail'], notif)
    content += StoryUnitData(SRPGMap, StageScenario, index)
    content += '==Other appearances==\n{{BattleAppearances}}\n\n'
    contentS = MapStory(mapId,['Book ' + ROMAN[StageScenario['book']]])
    if contentS == '':
        pass
    elif contentS[:8] == '{{#ifeq:' and contentS.find('}}}}<noinclude>') != -1:
        content += '==Story==\n' + contentS[48:contentS.find('}}}}')+2] + '\n' + StoryNavBar(mapId) + '\n'
        contentS = ''
    else:
        content += '==Story==\n{{/Story}}\n' + StoryNavBar(mapId) + '\n'
    content += mapUtil.InOtherLanguage('MID_STAGE_' + mapId)
    content += "{{Story Maps Navbox|book=" + str(StageScenario['book']) + "}}"

    if contentS == '': return { title: content }
    else: return {
        title: content,
        title+'/Story': contentS
    }

def ParalogueMap(mapId: str, StageScenario: dict=None, index: int=None, notif: str=''):
    if not StageScenario:
        for scenario in util.fetchFehData("Common/SRPG/StageScenario", None):
            for m in scenario['maps'][0]['scenarios']:
                if m['id_tag'][:-1] == mapId:
                    StageScenario = scenario
                    index = scenario['maps'][0]['scenarios'].index(m)
                    notif = "Special Heroes Summoning Event: " + util.getName('MID_CHAPTER_'+StageScenario['id_tag']) + " (Notification)"
                    answer = util.askFor(None, "Is the notification '"+notif+"'?")
                    notif = notif if (not answer or re.match('y|o|yes|oui|', answer, re.IGNORECASE)) else answer

    SRPGMap = [util.readFehData("Common/SRPGMap/" + Id + ".json") for Id in [StageScenario['maps'][i]['scenarios'][index]["id_tag"] for i in range(3)]]
    title = util.getName(mapId, False)

    SRPGMap[0]['field'].update({'player_pos': SRPGMap[0]['player_pos']})

    content = StoryMapInfobox(StageScenario, SRPGMap[0]['field'], mapId, index) + '\n'
    content += mapUtil.MapAvailability(StageScenario['avail'], notif)
    content += StoryUnitData(SRPGMap, StageScenario, index)
    content += '==Other appearances==\n{{BattleAppearances}}\n\n'
    contentS = MapStory(mapId)
    if contentS != '':
        content += '==Story==\n{{/Story}}\n' + StoryNavBar(mapId) + '\n'
    content += mapUtil.InOtherLanguage('MID_STAGE_' + mapId)
    content += "{{Paralogue Maps Navbox}}"

    if contentS != '': return { title: content, title+'/Story': contentS }
    else:         return { title: content }

def StoryGroup(groupId: str):
    StageScenario = util.fetchFehData("Common/SRPG/StageScenario")[groupId]
    notif = util.askFor(None, f"What is the notification for {util.getName(groupId)} ({groupId})?") or ''

    ret = {}
    for i in range(StageScenario['maps'][0]['scenario_count']):
        ret.update(StoryMap(StageScenario['maps'][0]['scenarios'][i]["id_tag"][:-1], StageScenario, i, notif))
    ret[util.getName(groupId, False) + '/Story'] = GroupStory(ret)
    ret[util.getName(groupId, False)] = '#REDIRECT [[Story Maps#' + re.sub(r'Book .*?,\s*', '', util.getName(groupId)) + ']]'
    return ret

def ParalogueGroup(groupId: str):
    StageScenario = util.fetchFehData("Common/SRPG/StageScenario")[groupId]
    notif = "Special Heroes Summoning Event: " + util.getName(StageScenario['id_tag'], False) + " (Notification)"
    answer = util.askFor(None, "Is the notification '"+notif+"'?")
    notif = notif = notif if (not answer or re.match('y|o|yes|oui|', answer, re.IGNORECASE)) else answer

    ret = {}
    for i in range(StageScenario['maps'][0]['scenario_count']):
        ret.update(ParalogueMap(StageScenario['maps'][0]['scenarios'][i]["id_tag"][:-1], StageScenario, i, notif))
    ret[util.getName(groupId, False) + '/Story'] = GroupStory(ret)
    ret[util.getName(groupId, False)] = '#REDIRECT [[Paralogue Maps#' + util.getName(groupId) + ']]'

    category = re.search(r'Scenario Navbar\|paralogue\|(.*?)\}\}', ret[util.getName(groupId, False) + '/Story'])[1]
    for k in ret:
        if k[-6:] == '/Story' and k != util.getName(groupId, False) + '/Story':
            ret[k] += f'<noinclude>[[Category:{category} scenarios]]</noinclude>'

    return ret

def UpdateStoryParalogueList(groupId: str):
    type = 'Paralogue' if groupId[1] == 'X' else 'Story'
    pages = wikiUtil.getPageContent([f'{type} Maps', f'Template:{type} Maps Navbox'])
    name = util.getName(groupId)
    nameCleaned = re.sub(r'Book .*?,\s*', '', name)
    ret = {}

    if pages[f'{type} Maps'].find(name) == -1:
        content = "===" + nameCleaned + "===\n"
        content += '{{See also|'+util.getName(groupId,False)+'/Story}}'
        content += "{{#invoke:MapList|byGroup|"+name+"}}\n"
        content += mapUtil.InOtherLanguage('MID_CHAPTER_' + groupId).replace('==', '====')
        ret[f'{type} Maps'] = re.sub(r"\s*(==Xenologues==|\{\{Battle Screen Navbox\}\})", "\n" + content + '\\1', pages[f'{type} Maps'], 1)
    
    if pages[f'Template:{type} Maps Navbox'].find(nameCleaned) == -1:
        count = re.search(r'\d+', nameCleaned)[0]
        content = f" |group{count}=[[{type} Maps#{nameCleaned}|{nameCleaned}]]\n"
        content += f" |list{count}=\n"
        i = 1
        while f"MID_STAGE_{'X' if groupId[1] == 'X' else 'S'}{groupId[2:]}{i}" in DATA:
            name = DATA[f"MID_STAGE_{'S' if groupId[1] == '0' else groupId[1]}{groupId[2:]}{i}"]
            content += f'# [[{name}]]\n'
            i += 1
        ret[f'Template:{type} Maps Navbox'] = re.sub(r'(\|group99|\}\}\}\})', content + '\\1', pages[f'Template:{type} Maps Navbox'], 1)
    
    return ret

from sys import argv

if __name__ == "__main__":
    for arg in argv[1:]:
        if re.match(r"^S\d{4}$", arg):
            print(StoryMap(arg))
        elif re.match(r"^C\d{4}$", arg):
            g = StoryGroup(arg)
            for name in g:
                print(name, g[name])
        elif re.match(r"^X\d{4}$", arg):
            print(ParalogueMap(arg))
        elif re.match(r"^CX\d{3}$", arg):
            g = ParalogueGroup(arg)
            for name in g:
                print(name, g[name])