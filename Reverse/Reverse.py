#!/usr/bin/env python3

from subprocess import Popen, PIPE, STDOUT
import json
import re

from datetime import datetime, date, timedelta
from os.path import dirname, realpath

import REutil as util

"""
BIN.LZ Header:
TotalSize: 0x04
CorpseSize: 0x04
PtrTableCount: 0x04 (0x08 each: adress of a pointer)
StringTableSize: 0x04 (0x08 each : corpseIdx (0x04), tblStringIdx (0x04))
XXX: 0x04
XXX: 0x04
"""

from RevData  import parseMsg
from RevSound import parseSound, parseStageBGM, parseStageBGMHeroTrial
from RevMap   import parseField, parseSRPGMap, parseStageScenario, parseStageEvent, parseStagePuzzle, parseSequentialTrialBind, parseSequentialTrialStory
from RevUnit  import parsePerson, parseEnemy, parseSubscriptionCostume
from RevQuest import parseMission
from RevSkill import parseSkill, parseSkillAccessory, parseSkillAccessoryCreatable, parseWeaponRefine, parseCaptainSkill

from RevVG  import parseTournament
from RevTT  import parseSequentialMap
from RevTB  import parseTapAction
from RevGC  import parseOccupation, parseWorld
from RevFB  import parsePortrait
from RevRS  import parseShadow
from RevLL  import parseTrip
from RevHoF import parseIdolTower
from RevMS  import parseMjolnir
from RevFP  import parseEncourage
from RevPoL import parseBoardGame
from RevHJ  import parseJourney
from RevSD  import parseRealTimePvPStage, parseRealTimePvPFave_Rate
from RevBW  import parseConnectBonds

def parseAccessory(data):
    result = []
    nbGroup = util.getLong(data,0x08, 0x0de4c6f0ab07e0e13)
    for iGr in range(nbGroup):
        offGr = util.getLong(data, 0x00) + 0x20*iGr
        result += [{
            "id_tag": util.getString(data, offGr+0x00),
            "sprite": util.getString(data, offGr+0x08),
            "id_num": util.getInt(data, offGr+0x10, 0xf765ad9c),
            "sort_id": util.getInt(data, offGr+0x14, 0x0159b21d),
            "acc_type": util.getInt(data, offGr+0x18, 0x8027f6f6),
            "summoner": util.getBool(data, offGr+0x1c, 0xB7)
        }]
    return result

def parseFriendDouble(data):#Allegiance Battle
    result = []
    nbGroup = util.getLong(data,0x08, 0xc0f4be0d2ed1f055)
    for iGr in range(nbGroup):
        offGr = util.getLong(data, 0x00) + 0xB0 * iGr
        result += [{
            "season_id": util.getString(data, offGr+0x00),
            "unit_id": util.getString(data, offGr+0x08),
            "map_id": util.getString(data, offGr+0x10),
            "a": hex(util.getLong(data, offGr+0x18)),
            #"avail": util.getAvail(data, offGr+0x20),
            #unknow 0x01
            #padding 0x07
            "origins": hex(util.getLong(data, offGr+0x48)),
            "origins1": hex(util.getLong(data, offGr+0x50)),
            "origins2": hex(util.getLong(data, offGr+0x58)),
            "origins3": hex(util.getLong(data, offGr+0x60)),
            "origins4": hex(util.getLong(data, offGr+0x68)),
            "origins5": hex(util.getLong(data, offGr+0x70)),
            "origins6": hex(util.getLong(data, offGr+0x78)),
            "origins7": hex(util.getLong(data, offGr+0x80)),
            "origins8": hex(util.getLong(data, offGr+0x88)),#ptr
            "origins9": hex(util.getLong(data, util.getLong(data, offGr+0x90)+0x04)),
            #"score_rewards": hex(util.getLong(data, offGr+0x98)),#tbl cellsize:0x18
            #"rank_rewards": hex(util.getLong(data, offGr+0xA0)),#tbl cellsize:0x18
            "origins12": hex(util.getLong(data, offGr+0xA8)),
        }]
    return result#data f13f93d09e0f5af7

def parseLoginBonus(data, strTbl):
    result = {
        '_id_tag': strTbl[0],
        'path': util.getString(data, 0x00, util.LOGIN_XORKEY)
    }
    return result

def parseSummon(data, strTbl):
    def getEncrypted(data, idx, size):
        off1 = util.getLong(data, idx)
        if off1 == 0: return None
        #print(hex(off1),[hex(data[i])[2:] for i in range(off1,off1+0x12)])
        return f"** Encrypted data: 0x{size:x} **" if util.getLong(data,idx) != 0 else None

    result = {
        'summons': [],
        'list2': [{} for i in range(util.getLong(data, 0x18))],
        'list3': [{} for i in range(util.getLong(data, 0x28))],
        'tips': [{
            "unit": util.getString(data, util.getLong(data, 0x30)+0x10*i, util.SUMMON_XORKEY),
            "tip": util.getString(data, util.getLong(data, 0x30)+0x10*i+0x08, util.SUMMON_XORKEY),
        } for i in range(util.getLong(data, 0x38))],
    }
    for iSumm in range(util.getLong(data, 0x08)):
        offSumm = util.getLong(data, 0x40+0x08*iSumm)
        result["summons"] += [{
            "_id_tag": strTbl[offSumm] if offSumm in strTbl else '',
            "banner_path": util.getString(data, offSumm+0x00, util.SUMMON_XORKEY),
            "character_lists": util.getString(data, offSumm+0x08, util.SUMMON_XORKEY),
            "ticket": util.getString(data, offSumm+0x10, util.SUMMON_XORKEY),
            "ticket_L": util.getString(data, offSumm+0x18, util.SUMMON_XORKEY),
            "red_classes": [util.getString(data, util.getLong(data, offSumm+0x20)+0x08*i, util.SUMMON_XORKEY) for i in range(util.getShort(data, offSumm+0xB4, 0x7E7E))],
            "blue_classes": [util.getString(data, util.getLong(data, offSumm+0x28)+0x08*i, util.SUMMON_XORKEY) for i in range(util.getShort(data, offSumm+0xB6, 0xCD24))],
            "green_classes": [util.getString(data, util.getLong(data, offSumm+0x30)+0x08*i, util.SUMMON_XORKEY) for i in range(util.getShort(data, offSumm+0xB8, 0xF66C))],
            "colorless_classes": [util.getString(data, util.getLong(data, offSumm+0x38)+0x08*i, util.SUMMON_XORKEY) for i in range(util.getShort(data, offSumm+0xBA, 0xA257))],

            "_enc01": getEncrypted(data, util.getLong(data, offSumm+0x40)+0x08, util.getInt(data, util.getLong(data, offSumm+0x40), 0xABFBA5F4)),
            "_enc02": getEncrypted(data, util.getLong(data, offSumm+0x48)+0x08, util.getInt(data, util.getLong(data, offSumm+0x48), 0xABFBA5F4)),
            "_enc03": getEncrypted(data, util.getLong(data, offSumm+0x50)+0x08, util.getInt(data, util.getLong(data, offSumm+0x50), 0xABFBA5F4)),
            "_enc04": getEncrypted(data, util.getLong(data, offSumm+0x58)+0x08, util.getInt(data, util.getLong(data, offSumm+0x58), 0xABFBA5F4)),
            "_enc05": getEncrypted(data, util.getLong(data, offSumm+0x60)+0x08, util.getInt(data, util.getLong(data, offSumm+0x60), 0xABFBA5F4)),
            "_enc06": getEncrypted(data, util.getLong(data, offSumm+0x68)+0x08, util.getInt(data, util.getLong(data, offSumm+0x68), 0xABFBA5F4)),
            "_enc07": getEncrypted(data, util.getLong(data, offSumm+0x70)+0x08, util.getInt(data, util.getLong(data, offSumm+0x70), 0xABFBA5F4)),
            "_enc08": getEncrypted(data, util.getLong(data, offSumm+0x78)+0x08, util.getInt(data, util.getLong(data, offSumm+0x78), 0xABFBA5F4)),
            "_enc09": getEncrypted(data, util.getLong(data, offSumm+0x80)+0x08, util.getInt(data, util.getLong(data, offSumm+0x80), 0xABFBA5F4)),
            "_enc10": getEncrypted(data, util.getLong(data, offSumm+0x88)+0x08, util.getInt(data, util.getLong(data, offSumm+0x88), 0xABFBA5F4)),

            "unit_tips": [{
                "unit": util.getString(data, util.getLong(data, offSumm+0x90)+0x10*i, util.SUMMON_XORKEY),
                "tip": util.getString(data, util.getLong(data, offSumm+0x90)+0x10*i+0x08, util.SUMMON_XORKEY),
            } for i in range(util.getByte(data, offSumm+0xBE, 0xFB))],
            "_focus": [
                getEncrypted(data, util.getLong(data, offSumm+0x98)+0x10*i+0x08, util.getInt(data, util.getLong(data, offSumm+0x98)+0x10*i, 0xA406A9D4))
            for i in range(util.getByte(data, offSumm+0xBF, 0x70))],
            "_encs2": {
                "_enc1": getEncrypted(data, util.getLong(data, offSumm+0xA0)+0x08, util.getInt(data, util.getLong(data, offSumm+0xA0)+0x00, 0xDAE3B02D)),
                "_enc2": getEncrypted(data, util.getLong(data, offSumm+0xA0)+0x10, util.getInt(data, util.getLong(data, offSumm+0xA0)+0x04, 0x30916DF7)),
            },
            "_enc12": getEncrypted(data, util.getLong(data, offSumm+0xA8)+0x08, util.getInt(data, util.getLong(data, offSumm+0xA8), 0xAD164F6D)),
            "_sort_id": util.getInt(data, offSumm+0xB0),
            "red_count": util.getShort(data, offSumm+0xB4, 0x7E7E),
            "blue_count": util.getShort(data, offSumm+0xB6, 0xCD24),
            "green_count": util.getShort(data, offSumm+0xB8, 0xF66C),
            "colorless_count": util.getShort(data, offSumm+0xBA, 0xA257),
            "_count1": util.getShort(data, offSumm+0xBC),
            "tipsCount": util.getByte(data, offSumm+0xBE, 0xFB),
            "focusCount": util.getByte(data, offSumm+0xBF, 0x70),
            "start": datetime.utcfromtimestamp(util.getLong(data, offSumm+0xC0, 0xD8D11EC213CC3ED1)).isoformat() + "Z" if util.getLong(data, offSumm+0xC0) != 0x0000000000000000 else None,
            "end": datetime.utcfromtimestamp(util.getLong(data, offSumm+0xC8, 0x4477DFA58F8520A7)).isoformat() + "Z" if util.getLong(data, offSumm+0xC8) not in [0x0000000000000000,0x3b88205a707adf58] else -1,
        }]
    return result

def parseBattleAsset(data):
    #return util.getAllStringsOn(data, util.BATTLE_XORKEY)
    getString = lambda data, off: util.getString(data, off, util.BATTLE_XORKEY)
    offGr = [util.getLong(data, 0x00+0x10*i) for i in range(6)]
    return {
        "unitData": [{
            "id_tag": getString(data, offGr[0]+0x90*iGr+0x00),
            # 0x18,
            "shorts": [hex(util.getShort(data, offGr[0]+0x90*iGr+0x08+0x02*i, 0x3FD0)) for i in range(0x0C)],
            "name1": getString(data, offGr[0]+0x90*iGr+0x20),
            # 0x08
            "ptr1?": util.getLong(data, offGr[0]+0x90*iGr+0x30),
            "name2": getString(data, offGr[0]+0x90*iGr+0x38),
            "name22": getString(data, offGr[0]+0x90*iGr+0x40),
            "name3": getString(data, offGr[0]+0x90*iGr+0x48),
            "name4": getString(data, offGr[0]+0x90*iGr+0x50),
            # 0x08
            "name5": getString(data, offGr[0]+0x90*iGr+0x60),
            "name6": getString(data, offGr[0]+0x90*iGr+0x68),
            "name7": getString(data, offGr[0]+0x90*iGr+0x70),
            # 0x08
            "ptr8?": util.getLong(data, offGr[0]+0x90*iGr+0x80),
            "ptr9?": util.getLong(data, offGr[0]+0x90*iGr+0x88),
            # DF BA DF 07
        } for iGr in range(util.getInt(data, 0x08))],
        "magicData": [{
            # 1C B2 31 2C
        } for iGr in range(util.getInt(data, 0x18))],
        "data3": [{

        } for iGr in range(util.getInt(data, 0x28))],
        "data4": [{

        } for iGr in range(util.getInt(data, 0x38))],
        "specialData": [{

        } for iGr in range(util.getInt(data, 0x48))],
        "weaponData": [{
            # 27 8B 20 61
        } for iGr in range(util.getInt(data, 0x58))],
        "strings": util.getAllStringsOn(data, util.BATTLE_XORKEY)
    }

def parseBattleBg(data):
    TYPES = ['Normal', 'Inside', 'Desert', 'Forest', 'Sea', 'Lava', 'Bridge', 'NormalWall', 'ForestWall', 'InsideWall', 'Fortress', 'River']
    result = []
    nbGroup = util.getLong(data, 0x08, 0x3926EEACBF6214E0)
    for iGr in range(nbGroup):
        offGr = util.getLong(data, 0x00) + 0x10 * iGr
        result += [{
            'id_tag': util.getString(data, offGr+0x00, util.NONE_XORKEY),
            'backgrounds': [{
                '_type': TYPES[i],
                'name': util.getString(data, util.getLong(data, util.getLong(data, offGr+0x08)+0x08*i), util.NONE_XORKEY),
                'unknow': util.getString(data, util.getLong(data, util.getLong(data, offGr+0x08)+0x08*i)+0x08, util.NONE_XORKEY),
            } for i in range(12)]
        }]
    return result


def parseMjolnirFacility(data):
    result = []
    nbGroup = util.getInt(data, 0x08, 0xC6AC9C0F)
    for iGr in range(nbGroup):
        offGr = util.getLong(data, 0x00) + 0x80 * iGr
        result += [{
            'id_tag': util.getString(data, offGr+0x00),
            'sprite': util.getString(data, offGr+0x08),
            'broken_sprite': util.getString(data, offGr+0x10),
            '_unknow1': util.getString(data, offGr+0x18),
            'next': util.getString(data, offGr+0x20),
            'prev': util.getString(data, offGr+0x28),
            '_unknow2': util.getString(data, offGr+0x30),
            '_group_id': util.getString(data, offGr+0x38),
            '_struct_id': util.getInt(data, offGr+0x40, 0x11aa991a),
            '_struct_id2': util.getInt(data, offGr+0x44, 0x3251464d),
            'terrain_id': util.getInt(data, offGr+0x48, 0xe24a11af),
            '_unknow5': util.getInt(data, offGr+0x4C, 0x4a13cf66),
            '_unknow5-': util.getInt(data, offGr+0x50, 0x7856dae3),
            'level': util.getInt(data, offGr+0x54, 0xc54d1c6e),
            'cost': util.getInt(data, offGr+0x58, 0x85da15bc),
            '_unknow7': hex(util.getInt(data, offGr+0x5c, 0x35e586df)),
            '_unknow8': hex(util.getInt(data, offGr+0x60, 0x8de0beb1)),
            'a0': util.getInt(data, offGr+0x64, 0x69c452d9),
            '_unknow9': hex(util.getInt(data, offGr+0x68, 0x743af47c)),
            'range': util.getInt(data, offGr+0x6c, 0x84ca17f),# > 10 ? xy-range : range
            '_isGateway': util.getByte(data, offGr+0x70, 0x57),
            'effect_type': util.getByte(data, offGr+0x71, 0x70),
            'range_type': util.getSByte(data, offGr+0x72, 0x4e),
            '_unknow10': util.getByte(data, offGr+0x73, 0x85),
            'turns': util.getByte(data, offGr+0x74, 0x2d),
            'showPanelNewUpgradAvailable': util.getBool(data, offGr+0x75, 0x62),
            '_unknow11': util.getByte(data, offGr+0x76, 0x2a),
            '_unknow11-': util.getByte(data, offGr+0x77, 0x7c),
            '_isGateway2': util.getByte(data, offGr+0x78, 0x38),
            'isBase': util.getByte(data, offGr+0x79, 0xa6),
            'isSummoner': util.getBool(data, offGr+0x7a, 0x1c),
            'isSummoner2': util.getByte(data, offGr+0x7b, 0x1c),
            'isSummoner3': util.getByte(data, offGr+0x7c, 0x1c),
            'isSummoner4': util.getByte(data, offGr+0x7d, 0x1c),
            '_padding': util.getShort(data, offGr+0x7e),
        }]
    return result

def parseSkyCastleConsumeItem(data):
    result = []
    nbGroup = util.getInt(data, 0x08, 0x995D773A)
    for iGr in range(nbGroup):
        offGr = util.getLong(data, 0x00) + 0x48 * iGr
        result += [{
            'id_tag': util.getString(data, offGr+0x00),
            'next': util.getString(data, offGr+0x08),
            'sprite': util.getString(data, offGr+0x10),
            'avail': util.getAvail(data, offGr+0x18),
            'category': util.getInt(data, offGr+0x40, 0x5F139C3F),
        }]
    return result

def parseReliancePerson(data):
    result = []
    nbGroup = util.getLong(data, 0x08, 0x35557B7A1E3464F2)
    for iGr in range(nbGroup):
        offGr = util.getLong(data, 0x00) + 0x28 * iGr
        result += [{
            'id_tag': util.getString(data, offGr+0x00),
            '_short1': util.getShort(data, offGr+0x08, 0xF42C),
            'nb_battles': util.getShort(data, offGr+0x0A, 0x9437),
            '_short3': util.getShort(data, offGr+0x0C, 0xD005),
            '_short4': util.getShort(data, offGr+0x0E, 0xDD9E),
            '_short5': util.getShort(data, offGr+0x10, 0xE71B),
            'foe_min_level': util.getShort(data, offGr+0x12, 0x7627),
            '_short7': util.getShort(data, offGr+0x14, 0x8F3F),
            'stats': util.getStat(data, offGr+0x18),
        }]
    return result

def parseReliancePlayer(data):
    result = []
    nbGroup = util.getLong(data, 0x08, 0xBF9208D441AB94AC)
    for iGr in range(nbGroup):
        offGr = util.getLong(data, 0x00) + 0x20 * iGr
        result += [{
            'id_tag': util.getString(data, offGr+0x00),
            '_short1': util.getShort(data, offGr+0x08, 0x783C),
            'next_level_battles': util.getShort(data, offGr+0x0A, 0xEBDE),
            '_short3': util.getShort(data, offGr+0x0C, 0xBD63),
            'foe_min_level': util.getShort(data, offGr+0x0E, 0xC2D6),
            'stats': util.getStat(data, offGr+0x10),
        }]
    return result

def parseHeroTrial(data, strTbl):
    result = []
    nbGroup = len(strTbl)
    for iGr in range(nbGroup):
        offGr = 0x08 + 0x10 * iGr
        result += [{
            'map_id': strTbl[offGr] if offGr in strTbl else '',
            'diff': util.getInt(data, offGr+0x00, 0x7CBD6A60),
            'dragonflowers': util.getInt(data, offGr+0x04, 0x292C2039),
            '_unknow1': util.getInt(data, offGr+0x08, 0x3C834DF8),
            '_unknow2': util.getInt(data, offGr+0x0C, 0x568DFE69),
        }]
    return result

def reverseFile(file: str):
    initS = util.decompress(file)
    if not initS:
        print("Failed to decompress: " + file.replace('\\','/').replace(util.BINLZ_ASSETS_DIR_PATH, '.../'))
        return
    s = initS[0x20:]
    strTbl = initS[0x20 + util.getInt(initS, 0x04) + util.getInt(initS, 0x08) * 0x08 + util.getInt(initS, 0x0C) * 0x08:]
    strTbl = {util.getInt(initS, 0x20 + util.getInt(initS, 0x04) + util.getInt(initS, 0x08)*0x08 + i*0x08): util.xorString(strTbl[util.getInt(initS, 0x20 + util.getInt(initS, 0x04) + util.getInt(initS, 0x08)*0x08 + i*0x08 + 0x04):][:0x30], util.NONE_XORKEY) for i in range(util.getInt(initS, 0x0C))}
    file = file.replace('\\','/')

    if file.find("/Message/") != -1:
        return parseMsg(s)
    elif file.find("/Mission/") != -1:
        return parseMission(s)
    elif file.find("/LoginBonus/T49QnDBP2/") != -1:
       return parseLoginBonus(s, strTbl)
    elif file.find("/Sound/arc/") != -1:
        return parseSound(s)
    elif file.find("/DressAccessory/Data/") != -1:
        return parseAccessory(s)
    elif file.find("/SRPG/Skill/") != -1:
        return parseSkill(s)
    elif file.find("/SRPG/WeaponRefine/") != -1:
        return parseWeaponRefine(s)
    elif file.find("/SRPG/SkillAccessory/") != -1:
        return parseSkillAccessory(s)
    elif file.find("/SRPG/SkillAccessoryCreatable/") != -1:
        return parseSkillAccessoryCreatable(s)
    elif file.find("/SRPG/RealTimePvP/CaptainSkill/") != -1:
        return parseCaptainSkill(s)
    elif file.find("/SRPG/Person/") != -1:
        return parsePerson(s)
    elif file.find("/SRPG/Enemy/") != -1:
        return parseEnemy(s)
    elif file.find("/SubscriptionCostume/") != -1:
        return parseSubscriptionCostume(s)

    elif file.find("/SRPG/Field/") != -1:
        return parseField(s)
    elif file.find("/SRPG/StageBgm/HeroTrial/") != -1:
        return parseStageBGMHeroTrial(s, strTbl)
    elif file.find("/SRPG/StageBgm/") != -1:
        return parseStageBGM(s, strTbl)
    elif file.find("/SRPG/StageScenario/") != -1:
        return parseStageScenario(s)
    elif file.find("/SRPG/StageEvent/") != -1:
        return parseStageEvent(s)
    elif file.find("/SRPG/StagePuzzle/") != -1:
        return parseStagePuzzle(s)
    elif re.search('/SRPG/SequentialTrial(Side|Main)Story/',file):
        return parseSequentialTrialStory(s)
    elif file.find('/SRPG/SequentialTrialBind/') != -1:
        return parseSequentialTrialBind(s)
    elif file.find("/SRPGMap/") != -1:
        return parseSRPGMap(s)

    elif file.find("/Tournament/") != -1:
        return parseTournament(s)
    elif file.find("/SRPG/SequentialMap/") != -1:
        return parseSequentialMap(s)
    elif file.find("/TapAction/TapBattleData/") != -1:
        return parseTapAction(s)
    elif file.find("/Occupation/Data/") != -1:
        return parseOccupation(s)
    elif file.find("/Occupation/World") != -1:
        return parseWorld(s)
    elif file.find("/Portrait/") != -1:
        return parsePortrait(s)
    elif file.find("/Shadow/") != -1:
        return parseShadow(s)
    elif file.find("/Trip/Terms/") != -1:
        return parseTrip(s)
    elif file.find("/SRPG/IdolTower/") != -1:
        return parseIdolTower(s)
    elif file.find("/Mjolnir/BattleData/") != -1:
        return parseMjolnir(s)
    elif file.find("/Encourage/") != -1:
        return parseEncourage(s)
    elif file.find("/SRPG/BoardGame/") != -1:
        return parseBoardGame(s)
    elif file.find("/Journey/Terms/") != -1:
        return parseJourney(s)
    elif file.find("/Common/SRPG/RealTimePvP/Stage/") != -1:
        return parseRealTimePvPStage(s)
    elif file.find("/Common/SRPG/RealTimePvP/Fave/") != -1:
        return parseRealTimePvPFave_Rate(s)
    elif file.find("/Common/SRPG/RealTimePvP/Rate/") != -1:
        return parseRealTimePvPFave_Rate(s)
    elif file.find("/Common/SRPG/RealTimePvP/AdvancedRate/") != -1:
        return parseRealTimePvPFave_Rate(s)
    elif file.find("/Common/SRPG/ConnectBonds/") != -1:
        return parseConnectBonds(s)
    
    elif file.find('/Summon/') != -1:
        return parseSummon(s, strTbl)
    elif file.find('/Battle/Asset/') != -1:
        return parseBattleAsset(s)
    elif file.find('/SRPG/BattleBg/') != -1:
        return parseBattleBg(s)
    elif file.find('/Mjolnir/FacilityData/') != -1:
        return parseMjolnirFacility(s)
    elif file.find('/SkyCastle/ConsumeItemData/') != -1:
        return parseSkyCastleConsumeItem(s)
    elif file.find('/SRPG/ReliancePerson.bin.lz') != -1:
        return parseReliancePerson(s)
    elif file.find('/SRPG/ReliancePlayer.bin.lz') != -1:
        return parseReliancePlayer(s)
    elif file.find('/HeroTrial/') != -1:
        return parseHeroTrial(s, strTbl)
    else:
        print("Unknow reversal method: " + file.replace(util.BINLZ_ASSETS_DIR_PATH, '...'))
        return

def reverseList(files: list):
    from os.path import exists
    for file in files:
        if file[-7:] != '.bin.lz': continue
        try:
            s = reverseFile(util.BINLZ_ASSETS_DIR_PATH + file)
            if not s or not exists(util.JSON_ASSETS_DIR_PATH + re.sub(r'[/\\][^/\\]+$','',file)):
                print('Ignored file ' + file)
            else:
                newFile = util.JSON_ASSETS_DIR_PATH + file.replace('.bin.lz', '.json')
                json.dump(s, open(newFile, 'w', encoding='utf-8'), indent=2, ensure_ascii=False)
                print("File writen: " + file.replace('.bin.lz','.json'))
        except KeyboardInterrupt:
            print('Ignored file ' + file)
        except:
            print('Error with ' + file)


from sys import argv
from os.path import dirname, realpath

srcDir = dirname(dirname(realpath(__file__)))
exec(open(srcDir+'/PersonalData.py', 'r').read())
if __name__ == "__main__":
    if len(argv) == 1:
        files = json.load(open(srcDir+'/jsons/newFiles.json', 'r')) + json.load(open(srcDir+'/jsons/changedFiles.json', 'r'))
        reverseList(sorted(f.replace('\\','/') for f in files))
    else:
        for arg in argv[1:]:
            s = reverseFile(arg)
            if s:
                print(json.dumps(s, indent=2, ensure_ascii=False))
