#!/usr/bin/env python3

import json
from os.path import isfile

import REutil as util

def parseRealTimePvPFave_Rate(data):
    result = []
    nbGroup = util.getLong(data, 0x08, 0x5C24DEA5CC489268)
    for iGr in range(nbGroup):
        offGr = util.getLong(data, 0x00) + 0x70 * iGr
        result += [{
            'id_tag': util.getString(data, offGr+0x00),
            'avail': util.getAvail(data, offGr+0x08),
            '_ptr1': {
                #hex(util.getLong(data, offGr+0x30))
                #0x18
                # 74 3E 51 E3 47 8B BC DA  5F BD BF 50 13 A5 3E 27  B7 92 8B D3 39 12 B2 C3
            },
            '_ptr2': {
                '_arr1': [util.getShort(data, util.getLong(data, util.getLong(data, offGr+0x38)+0x00), 0x1E0D)],
                '_arr2': [util.getSShort(data, util.getLong(data, util.getLong(data, offGr+0x38)+0x08)+0x02*i, 0x569C) for i in range(3)],
                '_arr3': [util.getShort(data, util.getLong(data, util.getLong(data, offGr+0x38)+0x10)+0x02*i, 0xE82A) for i in range(5)],
                # 0x2C
                # padding 0x04
            },
            '_arr1': [{
                '_str': util.getString(data, util.getLong(data, util.getLong(data, offGr+0x40))+0x18*i),
                'tiers': util.getLong(data, (util.getLong(data, util.getLong(data, offGr+0x40))+0x18*i+0x08)) == util.getLong(data, offGr+0x48) and "== Same as below ==" or "TODO: New object",
                # 0x07
                # 0B C9 79 FA BA AC B5
                # 84 C8 79 FA BB AC B5
            } for i in range(util.getInt(data, util.getLong(data, offGr+0x40)+0x08, 0x7B3AABF4))],
            'tiers': [{
                'tier': util.getInt(data, util.getLong(data, util.getLong(data, offGr+0x48))+0x18*i+0x00, 0x63E6B689),
                'based_glory': util.getInt(data, util.getLong(data, util.getLong(data, offGr+0x48))+0x18*i+0x04, 0x6C6CE0C0),
                '_int3': util.getInt(data, util.getLong(data, util.getLong(data, offGr+0x48))+0x18*i+0x08, 0xF036CF62),
                'defeat_lost': util.getSInt(data, util.getLong(data, util.getLong(data, offGr+0x48))+0x18*i+0x0C, 0x50F84663),
                '_int5': util.getSInt(data, util.getLong(data, util.getLong(data, offGr+0x48))+0x18*i+0x10, 0xBAFB37A5),
                # padding 0x04
            } for i in range(util.getInt(data, util.getLong(data, offGr+0x48)+0x08, 0xAB20D4AE))],
            'tier_rewards': [{
                'reward': util.getReward(data, util.getLong(data, util.getLong(data, offGr+0x50))+i*0x28+0x00, util.getInt(data, util.getLong(data, util.getLong(data, offGr+0x50))+i*0x28+0x08, 0xE8DD3CF9)),
                'payload': util.getInt(data, util.getLong(data, util.getLong(data, offGr+0x50))+i*0x28+0x08, 0xE8DD3CF9),
                'tier': util.getInt(data, util.getLong(data, util.getLong(data, offGr+0x50))+i*0x28+0x0C, 0xD200E204),
                'reward_ids': [util.getString(data, util.getLong(data, util.getLong(data, offGr+0x50))+i*0x28+0x10+0x08*j) for j in range(3)],
            } for i in range(util.getInt(data, util.getLong(data, offGr+0x50)+0x08, 0x1EEA0687))],
            'rank_rewards': [{
                'reward': util.getReward(data, util.getLong(data, util.getLong(data, offGr+0x58))+i*0x30+00, util.getInt(data, util.getLong(data, util.getLong(data, offGr+0x58))+i*0x30+0x08, 0x785F977F)),
                'payload': util.getInt(data, util.getLong(data, util.getLong(data, offGr+0x58))+i*0x30+0x08, 0x785F977F),
                'rank_hi': util.getInt(data, util.getLong(data, util.getLong(data, offGr+0x58))+i*0x30+0x0C, 0x0D70C748),
                'rank_lo': util.getSInt(data, util.getLong(data, util.getLong(data, offGr+0x58))+i*0x30+0x10, 0xDBDDF690),
                '_padding': util.getInt(data, util.getLong(data, util.getLong(data, offGr+0x58))+i*0x30+0x14),
                'reward_ids': [util.getString(data, util.getLong(data, util.getLong(data, offGr+0x58))+i*0x30+0x18+0x08*j) for j in range(3)],
            } for i in range(util.getInt(data, util.getLong(data, offGr+0x58)+0x08, 0x591810F7))] if util.getLong(data, offGr+0x58) != 0 else None,
            # Other datas: 0x10
        }]
    return result

def parseRealTimePvPStage(data):
    result = []
    nbGroup = util.getLong(data, 0x08, 0x47A2B2C786EA31FA)
    for iGr in range(nbGroup):
        offGr = util.getLong(data, 0x00) + 0x58 * iGr
        result += [{
            'id_tag': util.getString(data, offGr+0x00),
            'avail': util.getAvail(data, offGr+0x08),
            'fixed_map_id': util.getString(data, offGr+0x30),
            'random_map_ids': [util.getString(data, util.getLong(data, offGr+0x38)+0x08*i) for i in range(util.getByte(data, offGr+0x50, 0x67))],
            'captain_skills': [util.getInt(data, util.getLong(data, offGr+0x40)+0x04*i, 0x7EC25F4B) for i in range(util.getByte(data, offGr+0x51, 0x42))],
            'bonus_units': [util.getString(data, util.getLong(data, offGr+0x48)+0x08*i) for i in range(util.getByte(data, offGr+0x52, 0x5F))],
            'maps_count': util.getByte(data, offGr+0x50, 0x67),
            'skills_count': util.getByte(data, offGr+0x51, 0x42),
            'units_count': util.getByte(data, offGr+0x52, 0x5F),
            # Padding: 0x05
        }]
    return result

def reverseSummonerDuelsCommon(tag: str):
    fpath = util.BINLZ_ASSETS_DIR_PATH + "/Common/SRPG/RealTimePvP/Stage/" + tag + ".bin.lz"
    if isfile(fpath):
        data = util.decompress(fpath)
        return parseRealTimePvPStage(data[0x20:])

def reverseSummonerDuels(tag: str):
    fpath = util.BINLZ_ASSETS_DIR_PATH + "/Common/SRPG/RealTimePvP/Fave/" + tag + ".bin.lz"
    if isfile(fpath):
        data = util.decompress(fpath)
        return parseRealTimePvPFave_Rate(data[0x20:])

def reverseSummonerDuelsR(tag: str):
    fpath = util.BINLZ_ASSETS_DIR_PATH + "/Common/SRPG/RealTimePvP/Rate/" + tag + ".bin.lz"
    if isfile(fpath):
        data = util.decompress(fpath)
        return parseRealTimePvPFave_Rate(data[0x20:])

def reverseSummonerDuelsS(tag: str):
    fpath = util.BINLZ_ASSETS_DIR_PATH + "/Common/SRPG/RealTimePvP/AdvancedRate/" + tag + ".bin.lz"
    if isfile(fpath):
        data = util.decompress(fpath)
        return parseRealTimePvPFave_Rate(data[0x20:])