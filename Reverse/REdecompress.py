#!/usr/bin/env python3

def decompressBinLz(data):
    n = 0
    def readByte(): nonlocal n; n+=1; return int.from_bytes(bytes(data[n-1:n]), 'little')
    def readInt(): nonlocal n; n+=4; return int.from_bytes(bytes(data[n-4:n]), 'little')
    
    # Decrypt data
    header = data[:0x04]
    data = list(data[0x04:])
    xorseed = header[1] | (header[2] << 0o10) | (header[3] << 0o20)
    if header[0] == 0x04 and xorseed == len(data):
        xorkey = list((0x8083 * xorseed).to_bytes(8, 'little'))
        for i in range(len(data)):
            data[i] ^= xorkey[i%4]
            xorkey[i%4] ^= data[i]
        return data
    elif header[0] != 0x17 or data[0] != 0x11:
        print('Non LZ11 compressed file')
        return
    xorkey = list((0x8083 * xorseed).to_bytes(8, 'little'))
    for i in range(4, len(data)):
        data[i] ^= xorkey[i%4]
        xorkey[i%4] ^= data[i]
    fSize = readInt() >> 0o10
    if fSize == 0:
        fSize = readInt()

    # Decompress data
    mask = 1
    off = 0
    temp = [0x00] * 0x1000
    buff = []
    while len(buff) < fSize:
        if mask == 1:
            flags = readByte()
            mask <<= 7
        else: mask >>= 1

        # Simple append
        if (flags & mask) == 0:
            b = readByte()
            buff.append(b)
            temp[off] = b
            off = (off+1) % 0x1000
        # Copy previous data
        else:
            b = readByte()
            if (b >> 4) >= 2:
                bb = b
                l = (b >> 4) + 0x1
            elif (b >> 4) == 0:
                bb = ((b << 0o10) | readByte())
                l = ((bb >> 4) & 0xFF) + 0x11
            elif (b >> 4) == 1:
                bb = ((b << 0o20) | (readByte() << 0o10) | readByte())
                l = ((bb >> 4) & 0xFFFF) + 0x0111
            off2 = (((bb & 0x0F) << 0o10) | readByte()) + 0x01
            if off2 > len(buff):
                print('Invalid file format')
                return
            for i in range(l):
                b = temp[(off-off2) % 0x1000]
                buff.append(b)
                temp[off] = b
                off = (off+1) % 0x1000
    return buff

from sys import argv
if __name__ == '__main__':
    if len(argv) != 2:
        exit(0)
    open('dataDecompress.bin','bw').write(bytes(decompressBinLz(open(argv[1],'br').read())))
