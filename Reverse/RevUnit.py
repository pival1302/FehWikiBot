#!/usr/bin/env python3

from datetime import datetime

import REutil as util

def parsePerson(data):
    result = []
    nbGroup = util.getLong(data,0x08, 0xde51ab793c3ab9e1)
    for iGr in range(nbGroup):
        offGr = util.getLong(data, 0x00)+0x2A8*iGr
        result += [{
            "id_tag": util.getString(data, offGr),
            "roman": util.getString(data, offGr+0x08),
            "face_name": util.getString(data, offGr+0x10),
            "face_name2": util.getString(data, offGr+0x18),
            "legendary": {
                "duo_skill_id": util.getString(data, util.getLong(data, offGr+0x20)+0x00),
                "bonus_effect": util.getStat(data, util.getLong(data, offGr+0x20)+0x08),
                "kind": util.getByte(data, util.getLong(data, offGr+0x20)+0x18, 0x21),
                "element": util.getByte(data, util.getLong(data, offGr+0x20)+0x19, 0x05),
                "bst": util.getByte(data, util.getLong(data, offGr+0x20)+0x1A, 0x0F),
                "pair_up": util.getBool(data, util.getLong(data, offGr+0x20)+0x1B, 0x80),
                "ae_extra": util.getBool(data, util.getLong(data, offGr+0x20)+0x1C, 0x24),
                #padding 0x03
            } if util.getLong(data, offGr+0x20) != 0 else None,
            "dragonflower_costs": [util.getInt(data, util.getLong(data, util.getLong(data, offGr+0x28)+0x08)+0x04*i, 0x715C6A7B) for i in range(util.getInt(data, util.getLong(data, offGr+0x28), 0xA0013774))],
            "timestamp": (datetime.utcfromtimestamp(util.getLong(data, offGr+0x30, 0xBDC1E742E9B6489B)).isoformat() + "Z") if util.getLong(data, offGr+0x30, 0xBDC1E742E9B6489B) != 0xFFFFFFFFFFFFFFFF else None,
            "id_num": util.getInt(data, offGr+0x38, 0x5F6E4E18),
            "version_num": util.getInt(data, offGr+0x3C, 0x2E193A3C),
            "sort_value": util.getInt(data, offGr+0x40, 0x2A80349B),
            "origins": bin(util.getInt(data, offGr+0x44, 0xe664b808)),
            "weapon_type": util.getByte(data, offGr+0x48, 0x06),
            "tome_class": util.getByte(data, offGr+0x49, 0x35),
            "move_type": util.getByte(data, offGr+0x4A, 0x2A),
            "series": util.getByte(data, offGr+0x4B, 0x43),
            "random_pool": util.getByte(data, offGr+0x4C, 0xA1),
            "permanent_hero": util.getBool(data, offGr+0x4D, 0xC7),
            "base_vector_id": util.getByte(data, offGr+0x4E, 0x3D),
            "refresher": util.getBool(data, offGr+0x4F, 0xFF),
            #"unknow": util.getByte(data, offGr+0x50),
            #padding 0x07
            "base_stats": util.getStat(data, offGr+0x58),
            "growth_rates": util.getStat(data, offGr+0x68),
            "skills": [[util.getString(data, offGr+0x78+0x08*skill+0x08*14*rarity) for skill in range(14)] for rarity in range(5)]
        }]
    return result

def parseEnemy(data):
    result = []
    nbGroup = util.getLong(data,0x08, 0x62ca95119cc5345c)
    for iGr in range(nbGroup):
        offGr = util.getLong(data, 0x00)+0x78*iGr
        result += [{
            "id_tag": util.getString(data, offGr),
            "roman": util.getString(data, offGr+0x08),
            "face_name": util.getString(data, offGr+0x10),
            "face_name2": util.getString(data, offGr+0x18),
            "top_weapon": util.getString(data, offGr+0x20),
            "assist1": util.getString(data, offGr+0x28),
            "assist2": util.getString(data, offGr+0x30),
            "special": util.getString(data, offGr+0x38),
            "timestamp": (datetime.utcfromtimestamp(util.getLong(data, offGr+0x40, 0xBDC1E742E9B6489B)).isoformat() + "Z") if util.getLong(data, offGr+0x40, 0xBDC1E742E9B6489B) != 0xFFFFFFFFFFFFFFFF else None,
            "id_num": util.getInt(data, offGr+0x48, 0x422f41d4),
            "is_kiran": util.getInt(data, offGr+0x4C, 0x6D154FF7),
            "weapon_type": util.getByte(data, offGr+0x50, 0xe4),
            "tome_class": util.getByte(data, offGr+0x51, 0x81),
            "move_type": util.getByte(data, offGr+0x52, 0x0d),
            "random_allowed": util.getBool(data, offGr+0x53, 0xc4),
            "is_boss": util.getBool(data, offGr+0x54, 0x6A),
            "refresher": util.getBool(data, offGr+0x55, 0x2A),
            "is_enemy": util.getBool(data, offGr+0x57, 0x13),
            #padding 0x01
            "base_stats": util.getStat(data, offGr+0x58),
            "growth_rates": util.getStat(data, offGr+0x68),
        }]
    return result

def parseSubscriptionCostume(data):
    result = []
    nbGroup = util.getInt(data, 0x08, 0x17E55C54)
    for iGr in range(nbGroup):
        offGr = util.getLong(data, 0x00) + 0x20*iGr
        result += [{
            "id": util.getLong(data, offGr+0x00, 0xBFC98CB0CFDCD2D1),
            #"num": 
            "avail_start": datetime.utcfromtimestamp(util.getLong(data, offGr+0x08, 0x91f9ebb0b4a90f77)).isoformat() + "Z" if util.getLong(data, offGr+0x08) ^ 0x91f9ebb0b4a90f77 != 0xFFFFFFFFFFFFFFFF else None,
            "avail_finish": datetime.utcfromtimestamp(util.getLong(data, offGr+0x10, 0x75dc9cc8b9ece87f)).isoformat() + "Z" if util.getLong(data, offGr+0x10) ^ 0x75dc9cc8b9ece87f != 0xFFFFFFFFFFFFFFFF else None,
            "hero_id": util.getString(data, offGr+0x18)
        }]
    return result