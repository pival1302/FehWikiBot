#!/usr/bin/env python3

import json
from datetime import datetime
from os.path import isfile

import REutil as util

def parseSkill(data):
    result = []
    nbGroup = util.getLong(data,0x08, 0x7fecc7074adee9ad)
    for iGr in range(nbGroup):
        offGr = util.getLong(data, 0x00) + 0x160*iGr
        result += [{
            "id_tag": util.getString(data, offGr+0x00),
            "refine_base": util.getString(data, offGr+0x08),
            "name_id": util.getString(data, offGr+0x10),
            "desc_id": util.getString(data, offGr+0x18),
            "refine_id": util.getString(data, offGr+0x20),
            "beast_effect_id": util.getString(data, offGr+0x28),
            "prerequisites": [util.getString(data, offGr+0x30+0x08*i) for i in range(2)],
            "next_skill": util.getString(data, offGr+0x40),
            "sprites": [util.getString(data, offGr+0x48+0x08*i, util.NONE_XORKEY) for i in range(4)],
            "stats": util.getStat(data, offGr+0x68),
            "class_params": util.getStat(data, offGr+0x78),
            "combat_buffs": util.getStat(data, offGr+0x88),
            "skill_params": util.getStat(data, offGr+0x98),
            "skill_params2": util.getStat(data, offGr+0xA8),
            "refine_stats": util.getStat(data, offGr+0xB8),
            "id_num": util.getInt(data, offGr+0xC8, 0xc6a53a23),
            "sort_id": util.getInt(data, offGr+0xCC, 0x8DDBF8AC),
            "icon_id": util.getInt(data, offGr+0xD0, 0xC6DF2173),
            "wep_equip": util.getInt(data, offGr+0xD4, 0x35B99828),
            "mov_equip": util.getInt(data, offGr+0xD8, 0xAB2818EB),
            "sp_cost": util.getInt(data, offGr+0xDC, 0xC031F669),
            "category": util.getByte(data, offGr+0xE0, 0xBC),
            "tome_class": util.getByte(data, offGr+0xE1, 0xF1),
            "exclusive": util.getBool(data, offGr+0xE2, 0xCC),
            "enemy_only": util.getBool(data, offGr+0xE3, 0x4F),
            "range": util.getByte(data, offGr+0xE4, 0x56),
            "might": util.getByte(data, offGr+0xE5, 0xD2),
            "cooldown_count": util.getSByte(data, offGr+0xE6, 0x56),
            "assist_cd": util.getBool(data, offGr+0xE7, 0xF2),
            "healing": util.getBool(data, offGr+0xE8, 0x95),
            "skill_range": util.getByte(data, offGr+0xE9, 0x09),
            "score": util.getShort(data, offGr+0xEA, 0xA232),
            "promotion_tier": util.getByte(data, offGr+0xEC, 0xE0),
            "promotion_rarity": util.getByte(data, offGr+0xED, 0x75),
            "refined": util.getBool(data, offGr+0xEE, 0x02),
            "refine_sort_id": util.getByte(data, offGr+0xEF, 0xFC),
            "wep_effective": util.getInt(data, offGr+0xF0, 0x23BE3D43),
            "mov_effective": util.getInt(data, offGr+0xF4, 0x823FDAEB),
            "wep_shield": util.getInt(data, offGr+0xF8, 0xAABAB743),
            "mov_shield": util.getInt(data, offGr+0xFC, 0x0EBEF25B),
            "wep_weakness": util.getInt(data, offGr+0x100, 0x005A02AF),
            "mov_weakness": util.getInt(data, offGr+0x104, 0xB269B819),
            "wep_adaptive": util.getInt(data, offGr+0x110, 0x494E2629),
            "mov_adaptive": util.getInt(data, offGr+0x114, 0xEE6CEF2E),
            "timing_id": util.getInt(data, offGr+0x118, 0x9C776648),
            "ability_id": util.getInt(data, offGr+0x11C, 0x72B07325),
            "limits": [{
                "id": util.getInt(data, offGr+0x120+0x08*i, 0x0EBDB832),
                "params": [util.getSShort(data, offGr+0x120+0x08*i+0x04+0x02*j, 0xA590) for j in range(2)],
            } for i in range(2)],
            "target_wep": util.getInt(data, offGr+0x130, 0x409FC9D7),
            "target_mov": util.getInt(data, offGr+0x134, 0x6C64D122),
            "passive_next": util.getString(data, offGr+0x138),
            "timestamp": (datetime.utcfromtimestamp(util.getLong(data, offGr+0x140, 0xED3F39F93BFE9F51)).isoformat() + "Z") if util.getSLong(data, offGr+0x140, 0xED3F39F93BFE9F51) != -1 else None,
            "random_allowed": util.getByte(data, offGr+0x148, 0x10),
            "min_lv": util.getByte(data, offGr+0x149, 0x90),
            "max_lv": util.getByte(data, offGr+0x14A, 0x24),
            "tt_inherit_base": util.getBool(data, offGr+0x14B, 0x19),
            "random_mode": util.getByte(data, offGr+0x14C, 0xBE),
            "limit3_id": util.getInt(data, offGr+0x150, 0x0EBDB832),
            "limit3_params": [util.getSShort(data, offGr+0x150+0x04+0x02*j, 0xA590) for j in range(2)],
            "range_shape": util.getByte(data, offGr+0x158, 0x5C),
            "target_either": util.getBool(data, offGr+0x159, 0xA7),
            "distant_counter": util.getBool(data, offGr+0x15A, 0xDB),
            "canto_range": util.getByte(data, offGr+0x15B, 0x41),
            "pathfinder_range": util.getByte(data, offGr+0x15C, 0xBE),
            "arcane_weapon": util.getBool(data, offGr+0x15D, 0xAA),
        }]
    return result

def parseSkillAccessory(data):
    result = []
    nbGroup = util.getLong(data, 0x08, 0x166B3E4746A9D6D6)
    for iGr in range(nbGroup):
        offGr = util.getLong(data, 0x00) + 0x20 * iGr
        result += [{
            'id_tag': util.getString(data, offGr+0x00),
            'next_seal': util.getString(data, offGr+0x08),
            'prev_seal': util.getString(data, offGr+0x10),
            'ss_coin': util.getShort(data, offGr+0x18, 0xC540),
            'ss_badge_type': util.getShort(data, offGr+0x1A, 0xD50F),
            'ss_badge': util.getShort(data, offGr+0x1C, 0x8CEC),
            'ss_great_badge': util.getShort(data, offGr+0x1E, 0xCCFF),
        }]
    return result

def parseSkillAccessoryCreatable(data):
    result = []
    nbGroup = util.getLong(data,0x08, 0x0605B9F01A117E27)
    for iGr in range(nbGroup):
        offGr = util.getLong(data, 0x00) + 0x08 * iGr
        result += [{
            'id_tag': util.getString(data, offGr+0x00)
        }]
    return result

def parseWeaponRefine(data):
    result = []
    nbGroup = util.getLong(data,0x08, 0x45162C00432CFD73)
    for iGr in range(nbGroup):
        offGr = util.getLong(data, 0x00) + 0x20 * iGr
        result += [{
            'orig': util.getString(data, offGr+0x00),
            'refined': util.getString(data, offGr+0x08),
            'use': [{
                'res_type': util.getShort(data, offGr+0x10+i*0x04, 0x439C),
                'count': util.getShort(data, offGr+0x12+i*0x04, 0x7444),
            } for i in range(2)],
            'give': {
                'res_type': util.getShort(data, offGr+0x18, 0x439C),
                'count': util.getShort(data, offGr+0x1A, 0x7444),
            }
        }]
    return result

def parseCaptainSkill(data):
    result = []
    nbGroup = util.getLong(data,0x08, 0xB77037ED6EBCEC56)
    for iGr in range(nbGroup):
        offGr = util.getLong(data, 0x00) + 0x58 * iGr
        result += [{
            'id_tag': util.getString(data, offGr+0x00),
            #'stats1': util.getStat(data, offGr+0x08),
            #'_int1': util.getSInt(data, offGr+0x18, 0x2A4BF41A),
            #0x08
            # Padding 0x04
            #'stats2': util.getStat(data, offGr+0x28),
            #'_int2': util.getSInt(data, offGr+0x38, 0x2A4BF41A),
            #0x08
            # Padding 0x04
            'id_num': util.getInt(data, offGr+0x48, 0x54DC7C40),
            '_unknow1': util.getInt(data, offGr+0x4C, 0xC3D1B4BF),
            'icon_id': util.getInt(data, offGr+0x50, 0x7B344922),
        }]
    return result

def reverseSkill(tag: str):
    fpath = util.BINLZ_ASSETS_DIR_PATH + '/Common/SRPG/Skill/' + tag + '.bin.lz'
    if isfile(fpath):
        data = util.decompress(fpath)
        return parseSkill(data[0x20:])

def reverseSacredSeal(tag: str):
    fpath = util.BINLZ_ASSETS_DIR_PATH + '/Common/SRPG/SkillAccessory/' + tag + '.bin.lz'
    if isfile(fpath):
        data = util.decompress(fpath)
        return parseSkillAccessory(data[0x20:])
        
def reverseForgedSacredSeal(tag: str):
    fpath = util.BINLZ_ASSETS_DIR_PATH + '/Common/SRPG/SkillAccessoryCreatable/' + tag + '.bin.lz'
    if isfile(fpath):
        data = util.decompress(fpath)
        return parseSkillAccessoryCreatable(data[0x20:])

def reverseRefine(tag: str):
    fpath = util.BINLZ_ASSETS_DIR_PATH + '/Common/SRPG/WeaponRefine/' + tag + '.bin.lz'
    if isfile(fpath):
        data = util.decompress(fpath)
        return parseWeaponRefine(data[0x20:])

def reverseCaptainSkill(tag: str):
    fpath = util.BINLZ_ASSETS_DIR_PATH + '/Common/SRPG/RealTimePvP/CaptainSkill/' + tag + '.bin.lz'
    if isfile(fpath):
        data = util.decompress(fpath)
        return parseCaptainSkill(data[0x20:])