#!/usr/bin/env python3

import json
from os.path import isfile

import REutil as util

def parseConnectBonds(data):
    result = []
    for iGr in range(util.getLong(data, 0x08, 0xA2233D21B59538BB)):
        offGr = util.getLong(data, 0x00) + 0x00*iGr
        result += [{
            "id_tag": util.getString(data, offGr+0x00),
            "avail": util.getAvail(data, offGr+0x08),
            "init_heroes": {
                "heroes": [util.getString(data, util.getLong(data, offGr+0x30)+0x08*i) for i in range(4)],
                "lv": util.getByte(data, util.getLong(data, offGr+0x30)+0x20, 0x7D),
            },
            "_unknow1": {
                #0x0A
            },
            "enclosures": [{
                "id_tag": util.getString(data, util.getLong(data, util.getLong(data, offGr+0x40))+0x48*i+0x00),
                "id_num": util.getInt(data, util.getLong(data, util.getLong(data, offGr+0x40))+0x48*i+0x08, 0x476A7493),
                "hp_factor": util.getInt(data, util.getLong(data, util.getLong(data, offGr+0x40))+0x48*i+0x0C, 0xEB36D398),
                "min_hero_id": util.getInt(data, util.getLong(data, util.getLong(data, offGr+0x40))+0x48*i+0x10, 0x74C67B3D),#0,200,300,400,500
                "max_hero_id": util.getInt(data, util.getLong(data, util.getLong(data, offGr+0x40))+0x48*i+0x14, 0x33F7264A),
                "difficulty": util.getByte(data, util.getLong(data, util.getLong(data, offGr+0x40))+0x48*i+0x18, 0x53),
                "rarity": util.getByte(data, util.getLong(data, util.getLong(data, offGr+0x40))+0x48*i+0x19, 0x3B),
                "lv": util.getByte(data, util.getLong(data, util.getLong(data, offGr+0x40))+0x48*i+0x1A, 0x65),
                "generic_foes": util.getBool(data, util.getLong(data, util.getLong(data, offGr+0x40))+0x48*i+0x1B, 0x1E),
                "_unknow1": util.getBool(data, util.getLong(data, util.getLong(data, offGr+0x40))+0x48*i+0x1C, 0x30),
                "allow_refines": util.getBool(data, util.getLong(data, util.getLong(data, offGr+0x40))+0x48*i+0x1D, 0xDB),
                "_unknow2": util.getByte(data, util.getLong(data, util.getLong(data, offGr+0x40))+0x48*i+0x1E, 0x41),#CD 83
                "max_level": util.getByte(data, util.getLong(data, util.getLong(data, offGr+0x40))+0x48*i+0x1F, 0x10),
                "reward": util.getReward(data, util.getLong(data, util.getLong(data, offGr+0x40))+0x48*i+0x20, util.getInt(data, util.getLong(data, util.getLong(data, offGr+0x40))+0x48*i+0x28, 0xEB36D398)),
                #"payload": util.getInt(data, util.getLong(data, util.getLong(data, offGr+0x40))+0x48*i+0x28, 0xEB36D398),
                "reward_id": [util.getString(data, util.getLong(data, util.getLong(data, offGr+0x40))+0x48*i+0x30+0x08*j) for j in range(3)],
            } for i in range(util.getInt(data, util.getLong(data, offGr+0x40)+0x08, 0x50C874CC))],
            "daily_rewards": [{
                "reward": util.getReward(data, util.getLong(data, util.getLong(data, offGr+0x48))+0x28*i+0x00, util.getInt(data, util.getLong(data, util.getLong(data, offGr+0x48))+0x08, 0x33287F60)),
                "payload": util.getInt(data, util.getLong(data, util.getLong(data, offGr+0x48))+0x28*i+0x08, 0x33287F60),
                "reward_id": [util.getString(data, util.getLong(data, util.getLong(data, offGr+0x48))+0x28*i+0x10+0x08*j) for j in range(3)],
            } for i in range(util.getInt(data, util.getLong(data, offGr+0x48)+0x08, 0x35F7601A))],
        }]
    return result

def reverseBindingWorlds(tag: str):
    fpath = util.BINLZ_ASSETS_DIR_PATH + "/Common/SRPG/ConnectBonds/" + tag + ".bin.lz"
    if isfile(fpath):
        data = util.decompress(fpath)
        return parseConnectBonds(data[0x20:])


from sys import argv

if __name__ == "__main__":
    for arg in argv[1:]:
        s = reverseBindingWorlds(arg)
        if s:
            print(json.dumps(s, indent=2, ensure_ascii=False))