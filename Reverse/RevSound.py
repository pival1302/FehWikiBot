#!/usr/bin/env python3

from os.path import isfile

import REutil as util

getStringSound = lambda data, off: util.getString(data, off, util.SOUND_XORKEY)
getStringBgm = lambda data, off: util.getString(data, off, util.BGM_XORKEY)
from sys import stderr

def parseSound(data):
    nbGroup = util.getLong(data, 0x08)
    result = [None] * nbGroup
    for iGr in range(nbGroup):
        offGr = util.getLong(data, util.getLong(data, 0x00)+0x08*iGr)
        result[iGr] = {
            "id_tag": getStringSound(data, offGr+0x00),
            "count": util.getByte(data, offGr+0x08),
            "kind": util.getByte(data, offGr+0x09),#8 -> Simple music, 10 -> addition musics, 12 -> random music, 16 & 24 -> link to another music
            "_unknow3": hex(util.getShort(data, offGr+0x0A)),
            "audio_kind": util.getInt(data, offGr+0x0C),#0 -> BGM, 1 -> Sound, 2 -> Voice
            "list": [{
                "file": getStringSound(data, util.getLong(data, offGr+0x10+0x08*iMsc)+0x00),
                "archive": getStringSound(data, util.getLong(data, offGr+0x10+0x08*iMsc)+0x08),
                #"_ptr1": {
                #    "_unknow1": util.getInt(data, util.getLong(data, util.getLong(data, offGr+0x10+0x08*iMsc)+0x10)),
                #    "_unknow2": util.getInt(data, util.getLong(data, util.getLong(data, offGr+0x10+0x08*iMsc)+0x10)+0x04),
                #    "_unknow3": util.getInt(data, util.getLong(data, util.getLong(data, offGr+0x10+0x08*iMsc)+0x10)+0x08),
                #    "_unknow4": util.getInt(data, util.getLong(data, util.getLong(data, offGr+0x10+0x08*iMsc)+0x10)+0x0C),
                #    "_unknow5": util.getInt(data, util.getLong(data, util.getLong(data, offGr+0x10+0x08*iMsc)+0x10)+0x10),
                #    "_unknow6": util.getInt(data, util.getLong(data, util.getLong(data, offGr+0x10+0x08*iMsc)+0x10)+0x14),
                #},
                #"_ptr2": hex(util.getLong(data, util.getLong(data, offGr+0x10+0x08*iMsc)+0x18)),
                #"_ptr3": hex(util.getLong(data, util.getLong(data, offGr+0x10+0x08*iMsc)+0x20)),
                #"_unknow1": hex(util.getLong(data, util.getLong(data, offGr+0x10+0x08*iMsc)+0x28)),
                #"_unknow2": hex(util.getLong(data, util.getLong(data, offGr+0x10+0x08*iMsc)+0x30)),
                #"_unknow3": hex(util.getLong(data, util.getLong(data, offGr+0x10+0x08*iMsc)+0x38)),
                #"_unknow4": hex(util.getLong(data, util.getLong(data, offGr+0x10+0x08*iMsc)+0x40)),
            } for iMsc in range(util.getByte(data, offGr+0x08))],
        }
        if result[iGr]["kind"] == 16 or result[iGr]["kind"] == 24:
            for i in range(result[iGr]["count"]):
                result[iGr]["list"][i]["_ref"] = getStringSound(data, util.getLong(data, util.getLong(data, offGr+0x10+0x08*i)+0x00))
    return result

def parseStageBGM(data, strTbl):
    result = []
    for offset in sorted(strTbl.keys()):
        result += [{
            "id_tag": strTbl[offset] if offset in strTbl else '',
            "bgm_id": getStringBgm(data, offset),
            "bgm2_id": getStringBgm(data, offset+0x08),
            "unknow_id": getStringBgm(data, offset+0x10),
            "useGenericBossMusic": util.getBool(data, offset+0x18),
            "nbBossMusic": util.getInt(data, offset+0x19),
            "bossMusics": [{
                "boss": getStringBgm(data, offset+0x20+0x10*i),
                "bgm": getStringBgm(data, offset+0x28+0x10*i)
            } for i in range(util.getByte(data, offset+0x19))],
        }]
    return result

def parseStageBGMHeroTrial(data, strTbl):
    result = []
    nbGroup = util.getInt(data, 0x08, 0x00)
    for iGr in range(nbGroup):
        offGr = util.getLong(data, 0x00) + iGr*0x10
        result += [{
            'origin': strTbl[offGr] if offGr in strTbl else None,
            'map_bgm': getStringBgm(data, offGr+0x00),
            'battle_bgm': getStringBgm(data, offGr+0x08),
        }]
    return result

def reverseSound(tag: str):
    fpath = util.BINLZ_ASSETS_DIR_PATH + "/Common/Sound/arc/" + tag + ".bin.lz"
    if isfile(fpath):
        data = util.decompress(fpath)
        return parseSound(data[0x20:])

def reverseBGM(tag: str):
    fpath = util.BINLZ_ASSETS_DIR_PATH + "/Common/SRPG/StageBgm/" + tag + ".bin.lz"
    if isfile(fpath):
        data = util.decompress(fpath)
        strTbl = data[0x20 + util.getInt(data, 0x04) + util.getInt(data, 0x08) * 0x08 + util.getInt(data, 0x0C) * 0x08:]
        strTbl = {util.getInt(data, 0x20 + util.getInt(data, 0x04) + util.getInt(data, 0x08)*0x08 + i*0x08): util.xorString(strTbl[util.getInt(data, 0x20 + util.getInt(data, 0x04) + util.getInt(data, 0x08)*0x08 + i*0x08 + 0x04):][:0x30], util.NONE_XORKEY) for i in range(util.getInt(data, 0x0C))}
        return parseStageBGM(data[0x20:], strTbl)


import json
from sys import argv, stderr, exc_info
import traceback

if __name__ == "__main__":
    for arg in argv[1:]:
        try:
            s = reverseSound(arg)
            s2 = reverseBGM(arg)
            if s or s2:
                print(json.dumps(s, indent=2, ensure_ascii=False))
                print(json.dumps(s2, indent=2, ensure_ascii=False))
        except:
            t, v, tb = exc_info()
            print(f"Error: {arg}:", file=stderr)
            traceback.print_tb(tb)
            print(f"{t.__name__}: {v}", file=stderr)
            