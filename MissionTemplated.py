#! /usr/bin/env python3

import re
from datetime import date, datetime
from Reverse import Reverse

from util import TIME_FORMAT

def toTime(t): return datetime.strptime(t,TIME_FORMAT)
def elapsed(t0,t1): return (toTime(t1)-toTime(t0)).total_seconds()

SINGULAR_QUESTS = {
    r"(Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec) Quests": {
        'sort': 2,
        'timeChecker': lambda start,end: toTime(start).day == 1 and toTime(end).day == 1,
        'quests': [
            {'name': 'Arena Assault: Win 1', 'description': 'Win one battle in Arena Assault.', 'reward': r'\{kind=Hero Feather;count=1600\}'},
            {'name': 'Arena Assault: 3 in a Row', 'description': 'Win three consecutive battles<br>in Arena Assault.', 'reward': r'\{kind=Dueling Crest;count=7\}'},
            {'name': 'Arena Assault: 5 in a Row', 'description': 'Win five consecutive battles<br>in Arena Assault.', 'reward': r'\{kind=Dueling Crest;count=7\}'},
            {'name': 'KO w/Alfonse', 'description': 'Defeat a foe with Alfonse: Prince of Askr.', 'unit': 'Alfonse: Prince of Askr', 'reward': r'\{kind=Orb\}'},
            {'name': 'KO w/Sharena', 'description': 'Defeat a foe with Sharena: Princess of Askr.', 'unit': 'Sharena: Princess of Askr', 'reward': r'\{kind=Orb\}'},
            {'name': 'KO w/Anna', 'description': 'Defeat a foe with Anna: Commander.', 'unit': 'Anna: Commander', 'reward': r'\{kind=Orb\}'},
            {'name': 'KO Red Foe', 'description': 'Defeat a red foe.', 'times': 5, 'reward': r'\{kind=Orb\}'},
            {'name': 'KO Blue Foe', 'description': 'Defeat a blue foe.', 'times': 5, 'reward': r'\{kind=Orb\}'},
            {'name': 'KO Green Foe', 'description': 'Defeat a green foe.', 'times': 5, 'reward': r'\{kind=Orb\}'},
            {'name': 'KO Colorless Foe', 'description': 'Defeat a colorless foe.', 'times': 5, 'reward': r'\{kind=Orb\}'},
            {'name': 'KO Infantry Foe', 'description': 'Defeat an infantry foe.', 'times': 5, 'reward': r'\{kind=Aether Stone;count=20\}'},
            {'name': 'KO Armored Foe', 'description': 'Defeat an armored foe.', 'times': 5, 'reward': r'\{kind=Light\'s Blessing\}'},
            {'name': 'KO Cavalry Foe', 'description': 'Defeat a cavalry foe.', 'times': 5, 'reward': r'\{kind=Stamina Potion;count=3\}'},
            {'name': 'KO Flying Foe', 'description': 'Defeat a flying foe.', 'times': 5, 'reward': r'\{kind=Dueling Crest;count=3\}'},
            {'name': r'Clear \d+-1', 'description': r'Clear Book [IVX]+, Chapter \d+: Part 1 on Lunatic<br>difficulty. All four allies must survive.', 'reward': r'\{kind=Aether Stone;count=20\}'},
            {'name': r'Clear \d+-2', 'description': r'Clear Book [IVX]+, Chapter \d+: Part 2 on Lunatic<br>difficulty. All four allies must survive.', 'reward': r'\{kind=Light\'s Blessing\}'},
            {'name': r'Clear \d+-3', 'description': r'Clear Book [IVX]+, Chapter \d+: Part 3 on Lunatic<br>difficulty. All four allies must survive.', 'reward': r'\{kind=Stamina Potion;count=3\}'},
            {'name': r'Clear \d+-4', 'description': r'Clear Book [IVX]+, Chapter \d+: Part 4 on Lunatic<br>difficulty. All four allies must survive.', 'reward': r'\{kind=Dueling Crest;count=3\}'},
            {'name': r'Clear \d+-5', 'description': r'Clear Book [IVX]+, Chapter \d+: Part 5 on Lunatic<br>difficulty. All four allies must survive.', 'reward': r'\{kind=Aether Stone;count=20\}'},
            {'name': 'AR: Attack Successfully', 'description': 'Successfully attack in Aether Raids.', 'times': 3, 'reward': r'\{kind=Aether Stone;count=20\}'},
            {'name': 'AR: Attack Successfully', 'description': 'Successfully attack in Aether Raids.', 'times': 6, 'reward': r'\{kind=Stamina Potion;count=3\}'},
            {'name': 'AR: Attack Successfully', 'description': 'Successfully attack in Aether Raids.', 'times': 9, 'reward': r'\{kind=Dueling Crest;count=3\}'},
            {'name': 'Win Arena Duel', 'description': 'Win Arena Duel at any difficulty.', 'times': 5, 'reward': r'\{kind=Hero Feather;count=100\}'},
            {'name': 'Win Arena Duel', 'description': 'Win Arena Duel at any difficulty.', 'times': 10, 'reward': r'\{kind=Hero Feather;count=300\}'},
            {'name': 'Win Arena Duel', 'description': 'Win Arena Duel at any difficulty.', 'times': 15, 'reward': r'\{kind=Hero Feather;count=500\}'}
        ],
        'template': lambda group: '{{Monthly Quests Quest|story='+'-'.join(re.search(r'Clear Book ([IVX]+), Chapter (\d+)',group['quests'][14]['description']).group(1,2))+'}}'
    },
    r"(Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec) Training": {
        'sort': 2,
        'timeChecker': lambda start,end: toTime(start).day == 1 and toTime(end).day == 1,
        'quests': [
            {'name': 'Tower: 1st Stratum', 'description': 'Clear the first stratum of the Training Tower.<br>All four allies must survive.', 'times': 5, 'reward': r'\{kind=Hero Feather;count=100\}', 'stage': 'First Stratum'},
            {'name': 'Tower: 2nd Stratum', 'description': 'Clear the second stratum of the Training Tower.<br>All four allies must survive.', 'times': 5, 'reward': r'\{kind=Hero Feather;count=100\}', 'stage': 'Second Stratum'},
            {'name': 'Tower: 3rd Stratum', 'description': 'Clear the third stratum of the Training Tower.<br>All four allies must survive.', 'times': 5, 'reward': r'\{kind=Hero Feather;count=100\}', 'stage': 'Third Stratum'},
            {'name': 'Tower: 4th Stratum', 'description': 'Clear the fourth stratum of the Training Tower.<br>All four allies must survive.', 'times': 5, 'reward': r'\{kind=Hero Feather;count=100\}', 'stage': 'Fourth Stratum'},
            {'name': 'Tower: 5th Stratum', 'description': 'Clear the fifth stratum of the Training Tower.<br>All four allies must survive.', 'times': 10, 'reward': r'\{kind=Orb\}', 'stage': 'Fifth Stratum'},
            {'name': 'Tower: 6th Stratum', 'description': 'Clear the sixth stratum of the Training Tower.<br>All four allies must survive.', 'times': 10, 'reward': r'\{kind=Orb\}', 'stage': 'Sixth Stratum'},
            {'name': 'Tower: 7th Stratum', 'description': 'Clear the seventh stratum of the Training<br>Tower. All four allies must survive.', 'times': 10, 'reward': r'\{kind=Orb\}', 'stage': 'Seventh Stratum'},
            {'name': 'Tower: 8th Stratum', 'description': 'Clear the eighth stratum of the Training Tower.<br>All four allies must survive.', 'times': 15, 'reward': r'\{kind=Orb\}', 'stage': 'Eighth Stratum'},
            {'name': 'Tower: 9th Stratum', 'description': 'Clear the ninth stratum of the Training Tower.<br>All four allies must survive.', 'times': 15, 'reward': r'\{kind=Orb\}', 'stage': 'Ninth Stratum'},
            {'name': 'Tower: 10th Stratum', 'description': 'Clear the tenth stratum of the Training Tower.<br>All four allies must survive.', 'times': 15, 'reward': r'\{kind=Orb\}', 'stage': 'Tenth Stratum'},
            {'name': 'Tower: 10th Stratum', 'description': 'Clear the tenth stratum of the Training Tower<br>using only infantry allies. \\(Allies deployed as<br>cohorts using Pair Up do not count.\\) All four<br>allies must survive.', 'times': 15, 'reward': r'\{kind=Orb\}', 'stage': 'Tenth Stratum'},
            {'name': 'Tower: 10th Stratum', 'description': 'Clear the tenth stratum of the Training Tower<br>using only armored allies. \\(Allies deployed as<br>cohorts using Pair Up do not count.\\) All four<br>allies must survive.', 'times': 15, 'reward': r'\{kind=Orb\}', 'stage': 'Tenth Stratum'},
            {'name': 'Tower: 10th Stratum', 'description': 'Clear the tenth stratum of the Training Tower<br>using only cavalry allies. \\(Allies deployed as<br>cohorts using Pair Up do not count.\\) All four<br>allies must survive.', 'times': 15, 'reward': r'\{kind=Orb\}', 'stage': 'Tenth Stratum'},
            {'name': 'Tower: 10th Stratum', 'description': 'Clear the tenth stratum of the Training Tower<br>using only flying allies. \\(Allies deployed as<br>cohorts using Pair Up do not count.\\) All four<br>allies must survive.', 'times': 15, 'reward': r'\{kind=Orb\}', 'stage': 'Tenth Stratum'}
        ],
        'template': lambda _: '{{Monthly Training Quest}}'
    },
    'RB': {
        'sort': 3,
        'avail': 604800,
        'cycle': 1209600,
        'quests': [
            {'name': 'Resonant Battles Score', 'description': 'Earn a score in Resonant Battles<br>at any difficulty.', 'reward': r'\{kind=Orb\}'}, 
        ],
        'template': lambda group: '{{RB Quest|start='+group['startTime']+'|end='+group['endTime']+'}}'
    },
    'RB / AA': {
        'sort': 3,
        'avail': 604800,
        'cycle': 1209600,
        'quests': [
            {'name': 'Resonant Battles Score', 'description': 'Earn a score in Resonant Battles<br>at any difficulty.', 'reward': r'\{kind=Orb\}'}, 
            {'name': 'Arena Assault: Win 1', 'description': 'Win one battle in Arena Assault.', 'reward': r'\{kind=Hero Feather;count=400\}'}, 
            {'name': 'Arena Assault: 2 in a Row', 'description': 'Win two consecutive battles in Arena Assault.', 'reward': r'\{kind=Dueling Crest;count=7\}'}
        ],
        'template': lambda group: '{{RB AA Quest|start='+group['startTime']+'|end='+group['endTime']+'}}'
    },
    'RB / AB': {
        'sort': 3,
        'avail': 604800,
        'cycle': 1209600,
        'quests': [
            {'name': 'Resonant Battles Score', 'description': 'Earn a score in Resonant Battles<br>at any difficulty.', 'reward': r'\{kind=Orb\}'}, 
            {'name': 'Win Allegiance Battles', 'description': 'Win Allegiance Battles on any difficulty.', 'reward': r'\{kind=Hero Feather;count=800\}'}, 
        ],
        'template': lambda group: '{{RB AB Quest|start='+group['startTime']+'|end='+group['endTime']+'}}'
    },
    'RB / AB / AA': {
        'sort': 3,
        'avail': 604800,
        'cycle': 1209600,
        'quests': [
            {'name': 'Resonant Battles Score', 'description': 'Earn a score in Resonant Battles<br>at any difficulty.', 'reward': r'\{kind=Orb\}'}, 
            {'name': 'Win Allegiance Battles', 'description': 'Win Allegiance Battles on any difficulty.', 'reward': r'\{kind=Hero Feather;count=800\}'}, 
            {'name': 'Arena Assault: Win 1', 'description': 'Win one battle in Arena Assault.', 'reward': r'\{kind=Hero Feather;count=400\}'}
        ],
        'template': lambda group: '{{RB AB AA Quest|start='+group['startTime']+'|end='+group['endTime']+'}}'
    },
    r"\s*Feh Pass": {
        'sort': 50,
        'timeChecker': lambda start,end: (toTime(start).day, toTime(end).day) in [(10,25),(25,10)],
        'quests': [
            {'name': 'KO Foe', 'description': 'Defeat a foe.', 'times': 3, 'reward': r'\{kind=Orb;count=3\}'}, 
            {'name': 'KO Foe', 'description': 'Defeat a foe.', 'times': 30, 'reward': r'\{kind=Orb;count=2\}'}, 
            {'name': 'Win Arena Duels', 'description': 'Win Arena Duels at any difficulty.', 'reward': r'\{kind=Divine Code: Part \d+;count=120\}'}, 
            {'name': 'Win Arena Duels', 'description': 'Win Arena Duels at any difficulty.', 'times': 3, 'reward': r'\{kind=Divine Dew;count=35\}'}, 
            {'name': 'KO Armored Foe', 'description': 'Defeat an armored foe.', 'reward': r'\{kind=Heroic Grail;count=50\}'}, 
            {'name': 'KO Flying Foe', 'description': 'Defeat a flying foe.', 'reward': r'\{kind=Aether Stone;count=120\}'}
        ],
        'template': lambda group: f"{{{{Feh Pass Quest|start={group['startTime']}|end={group['endTime']}|Divine Code={group['quests'][2]['reward'][19:-11]}}}}}"
    },
    # Story Maps
    ".*": {
        'sort': 10,
        'timeChecker': lambda start,end: True,
        'quests': [
            {'name': r'Clear \d+-1 on Lunatic', 'description': r'Clear Book [IVX]+, Chapter \d+: Part 1 on Lunatic<br>difficulty with (a sword|a lance|an axe) ally on your team. \(Allies<br>deployed as cohorts using Pair Up do not<br>count.\) All four allies must survive.', 'reward': r'\{kind=Orb\}'},
            {'name': r'Clear \d+-2 on Lunatic', 'description': r'Clear Book [IVX]+, Chapter \d+: Part 2 on Lunatic<br>difficulty with (a sword|a lance|an axe) ally on your team. \(Allies<br>deployed as cohorts using Pair Up do not<br>count.\) All four allies must survive.', 'reward': r'\{kind=Orb\}'},
            {'name': r'Clear \d+-3 on Lunatic', 'description': r'Clear Book [IVX]+, Chapter \d+: Part 3 on Lunatic<br>difficulty with (a sword|a lance|an axe) ally on your team. \(Allies<br>deployed as cohorts using Pair Up do not<br>count.\) All four allies must survive.', 'reward': r'\{kind=Orb\}'},
            {'name': r'Clear \d+-4 on Lunatic', 'description': r'Clear Book [IVX]+, Chapter \d+: Part 4 on Lunatic<br>difficulty with (a sword|a lance|an axe) ally on your team. \(Allies<br>deployed as cohorts using Pair Up do not<br>count.\) All four allies must survive.', 'reward': r'\{kind=Orb\}'},
            {'name': r'Clear \d+-5 on Lunatic', 'description': r'Clear Book [IVX]+, Chapter \d+: Part 5 on Lunatic<br>difficulty with (a sword|a lance|an axe) ally on your team. \(Allies<br>deployed as cohorts using Pair Up do not<br>count.\) All four allies must survive.', 'reward': r'\{kind=Orb\}'}
        ],
        'template': lambda group: \
            '{{Story Maps Quest|story='+'-'.join(re.search(r'Clear Book ([IVX]+), Chapter (\d+)',group['quests'][0]['description']).group(1,2))+'|title='+group['title']+\
            '|usedWeapon=' + {'sword,lance,axe,sword,lance':'1','lance,axe,sword,lance,axe':'2','axe,sword,lance,axe,sword':'3'}[','.join([re.search('with an? (sword|lance|axe)', group['quests'][i]['description'])[1] for i in range(5)])]+\
            '|start='+group['startTime']+'|end='+group['endTime']+'}}'
    },
    # Paralogue Maps
    "..*": {
        'sort': 11,
        'timeChecker': lambda start,end: True,
        'quests': [
            {'name': r'Clear P\d+-1 on Lunatic', 'description': r'Clear Paralogue \d+: Part 1 on Lunatic difficulty<br>with (a sword|a lance|an axe) ally on your team\. \(Allies deployed<br>as cohorts using Pair Up do not count\.\) All four<br>allies must survive\.', 'reward': r'\{kind=Orb\}'},
            {'name': r'Clear P\d+-2 on Lunatic', 'description': r'Clear Paralogue \d+: Part 2 on Lunatic difficulty<br>with (a sword|a lance|an axe) ally on your team\. \(Allies deployed<br>as cohorts using Pair Up do not count\.\) All four<br>allies must survive\.', 'reward': r'\{kind=Orb\}'},
            {'name': r'Clear P\d+-3 on Lunatic', 'description': r'Clear Paralogue \d+: Part 3 on Lunatic difficulty<br>with (a sword|a lance|an axe) ally on your team\. \(Allies deployed<br>as cohorts using Pair Up do not count\.\) All four<br>allies must survive\.', 'reward': r'\{kind=Orb\}'}
        ],
        'template': lambda group: \
            '{{Paralogue Maps Quest|paralogue='+group['quests'][0]['name'][7:-13]+'|title='+group['title']+\
            '|usedWeapon=' + {'sword,lance,axe':'1','lance,axe,sword':'2','axe,sword,lance':'3'}[','.join([re.search('with an? (sword|lance|axe)', group['quests'][i]['description'])[1] for i in range(3)])]+\
            '|start='+group['startTime']+'|end='+group['endTime']+'}}'
    },

    'Voting Gauntlet': {
        'sort': 15,
        'avail': 172800,
        'count': 3,
        'quests': [
            {'name':'Voting Gauntlet','description':'Win a battle in the Voting Gauntlet.','reward':r'\{kind=Orb;count=2\}'},
            {'name':'Voting Gauntlet','description':'Win a battle in the Voting Gauntlet.','times':2,'reward':r'\{kind=Sacred Coin;count=30\}'},
            {'name':'Voting Gauntlet','description':'Win a battle in the Voting Gauntlet.','times':3,'reward':r'\{kind=Refining Stone;count=10\}'},
            {'name':'Voting Gauntlet','description':'Win a battle in the Voting Gauntlet.','times':4,'reward':r'\{kind=Sacred Coin;count=15\}'},
            {'name':'Voting Gauntlet','description':'Win a battle in the Voting Gauntlet.','times':5,'reward':r'\{kind=Orb;count=2\}'},
            {'name':'Tower: 2nd Stratum','description':'Clear the second stratum of the Training Tower.','times':5,'reward':r'\{kind=Battle Flag;count=100\}','stage':'Second Stratum'},
            {'name':'Tower: 5th Stratum','description':'Clear the fifth stratum of the Training Tower.','times':5,'reward':r'\{kind=Battle Flag;count=100\}','stage':'Fifth Stratum'},
            {'name':'Tower: 8th Stratum','description':'Clear the eighth stratum of the Training Tower.','times':5,'reward':r'\{kind=Battle Flag;count=100\}','stage':'Eighth Stratum'},
            {'name':'Win Arena Duels','description':'Win Arena Duels at any difficulty.','reward':r'\{kind=Battle Flag;count=100\}'},
            {'name':'Win Arena Duels','description':'Win Arena Duels at any difficulty.','times':2,'reward':r'\{kind=Battle Flag;count=100\}'},
            {'name':'Win Int.\\+ Arena Duels','description':'Win Arena Duels on Intermediate or higher<br>difficulty.','times':3,'reward':r'\{kind=Battle Flag;count=100\}'},
            {'name':'Win Int.\\+ Arena Duels','description':'Win Arena Duels on Intermediate or higher<br>difficulty.','times':5,'reward':r'\{kind=Battle Flag;count=100\}'},
            {'name':'Voting Gauntlet','description':'Win a battle in the Voting Gauntlet with a red<br>ally on your team.','reward':r'\{kind=Battle Flag;count=200\}'},
            {'name':'Voting Gauntlet','description':'Win a battle in the Voting Gauntlet with a blue<br>ally on your team.','reward':r'\{kind=Battle Flag;count=200\}'},
            {'name':'Voting Gauntlet','description':'Win a battle in the Voting Gauntlet with a green<br>ally on your team.','reward':r'\{kind=Battle Flag;count=200\}'},
            {'name':'Voting Gauntlet','description':'Win a battle in the Voting Gauntlet with a<br>colorless ally on your team.','reward':r'\{kind=Battle Flag;count=200\}'},
            {'name':'Voting Gauntlet','description':'Win a battle in the Voting Gauntlet with a red<br>ally on your team.','times':2,'reward':r'\{kind=Divine Code: Ephemera \d+;count=10}'},
            {'name':'Voting Gauntlet','description':'Win a battle in the Voting Gauntlet with a blue<br>ally on your team.','times':2,'reward':r'\{kind=Divine Code: Ephemera \d+;count=10}'},
            {'name':'Voting Gauntlet','description':'Win a battle in the Voting Gauntlet with a green<br>ally on your team.','times':2,'reward':r'\{kind=Divine Code: Ephemera \d+;count=10}'},
            {'name':'Voting Gauntlet','description':'Win a battle in the Voting Gauntlet with a<br>colorless ally on your team.','times':2,'reward':r'\{kind=Divine Code: Ephemera \d+;count=10}'},
            {'name':'Voting Gauntlet','description':'Win a battle in the Voting Gauntlet with a red<br>ally on your team.','times':3,'reward':r'\{kind=Battle Flag;count=100\}'},
            {'name':'Voting Gauntlet','description':'Win a battle in the Voting Gauntlet with a blue<br>ally on your team.','times':3,'reward':r'\{kind=Battle Flag;count=100\}'},
            {'name':'Voting Gauntlet','description':'Win a battle in the Voting Gauntlet with a green<br>ally on your team.','times':3,'reward':r'\{kind=Battle Flag;count=100\}'},
            {'name':'Voting Gauntlet','description':'Win a battle in the Voting Gauntlet with a<br>colorless ally on your team.','times':3,'reward':r'\{kind=Battle Flag;count=100\}'}
        ],
        'template': lambda group: f"{{{{Voting Gauntlet Quest|start={group['startTime']}|Divine Code={group['quests'][16]['reward'][19:-10]}|stage={group['quests'][0]['stage']}}}}}"
    },
    'Grand Conquests': {
        'sort': 18,
        'avail': 172800,
        'count': 3,
        'quests': [
            {'name':'GC: Earn Score','description':'Earn points in Grand Conquests.','reward':r'\{kind=Orb\}'},
            {'name':'GC: Earn Score','description':'Earn points in Grand Conquests.','times':2,'reward':r'\{kind=Universal Shard;count=1500\}'},
            {'name':'GC: Earn Score','description':'Earn points in Grand Conquests.','times':3,'reward':r'\{kind=Hero Feather;count=150\}'},
            {'name':'Tower: 2nd Stratum','description':'Clear the second stratum of the Training Tower.','times':5,'reward':r'\{kind=Conquest Lance\}','stage':'Second Stratum'},
            {'name':'Tower: 8th Stratum','description':'Clear the eighth stratum of the Training Tower.','times':5,'reward':r'\{kind=Conquest Lance\}','stage':'Eighth Stratum'},
            {'name':'Win Arena Duels','description':'Win Arena Duels on any difficulty.','times':5,'reward':r'\{kind=Conquest Lance\}'}
        ],
        'template': lambda group: f"{{{{Grand Conquests Quest|start={group['startTime']}|stage={group['quests'][0]['stage']}}}}}"
    },
    'Røkkr Sieges': {
        'sort': 20,
        'avail': 172800,
        'count': 3,
        'quests': [
            {'name':'Røkkr Sieges','description':'Clear a battle in Røkkr Sieges.','reward':r'\{kind=Orb\}'},
            {'name':'Røkkr Sieges','description':'Clear a battle in Røkkr Sieges.','times':2,'reward':r'\{kind=Universal Shard;count=1500\}'},
            {'name':'Røkkr Sieges','description':'Clear a battle in Røkkr Sieges.','times':3,'reward':r'\{kind=Hero Feather;count=150\}'},
            {'name':'Røkkr Sieges','description':'Clear a battle in Røkkr Sieges.','reward':r'\{kind=Divine Code: Ephemera \d+;count=10\}'},
            {'name':'Røkkr Sieges','description':'Clear a battle in Røkkr Sieges.','times':2,'reward':r'\{kind=Divine Code: Ephemera \d+;count=10\}'},
            {'name':'Røkkr Sieges','description':'Clear a battle in Røkkr Sieges.','times':3,'reward':r'\{kind=Divine Code: Ephemera \d+;count=10\}'},
            {'name':'Røkkr Sieges','description':'Clear a battle in Røkkr Sieges.','times':4,'reward':r'\{kind=Divine Code: Ephemera \d+;count=10\}'},
            {'name':'Tower: 2nd Stratum','description':'Clear the second stratum of the Training Tower.','times':5,'reward':r'\{kind=Havoc Axe\}','stage':'Second Stratum'},
            {'name':'Tower: 8th Stratum','description':'Clear the eighth stratum of the Training Tower.','times':5,'reward':r'\{kind=Havoc Axe\}','stage':'Eighth Stratum'},
            {'name':'Win Arena Duels','description':'Win Arena Duels at any difficulty.','times':5,'reward':r'\{kind=Havoc Axe\}'}
        ],
        'template': lambda group: f"{{{{Rokkr Siege Quest|start={group['startTime']}|Divine Code={group['quests'][3]['reward'][19:-10]}|stage={group['quests'][0]['stage']}}}}}"
    },
    "Mjölnir's Strike": {
        'sort': 23,
        'avail': 172800,
        'quests': [
            {'name': 'KO w/Summoner', 'description': "Defeat a foe with the My Summoner unit<br>in Mjölnir's Strike.", 'reward': r"\{kind=Divine Code: Part \d+;count=20\}", 'unit': 'PID_アバター'},
            {'name': 'KO w/Summoner', 'description': "Defeat a foe with the My Summoner unit<br>in Mjölnir's Strike.", 'times': 5, 'reward': r"\{kind=Divine Code: Part \d+;count=40\}", 'unit': 'PID_アバター'}
        ],
        'template': lambda group: f"{{{{Mjolnirs Strike Quest|start={group['startTime']}|Divine Code={group['quests'][0]['reward'][19:-10]}}}}}"
    },
    'Frontline Phalanx': {
        'sort': 24,
        'avail': 864000,
        'quests': [
            {'name':'Tower: 1st Stratum','description':'Clear the first stratum of the Training Tower.','reward':r'\{kind=Guardian Shield\}','stage':'First Stratum'},
            {'name':'Tower: 4th Stratum','description':'Clear the fourth stratum of the Training Tower.','reward':r'\{kind=Guardian Shield\}','stage':'Fourth Stratum'},
            {'name':'Tower: 7th Stratum','description':'Clear the seventh stratum of the<br>Training Tower.','reward':r'\{kind=Guardian Shield\}','stage':'Seventh Stratum'},
        ],
        'template': lambda group: '{{Frontline Phalanx Quest|start='+group['startTime']+'}}'
    },
    'Pawns of Loki': {
        'sort': 25,
        'avail': 172800,
        'quests': [
            {'name': 'PoL: Complete a Game', 'description': 'Finish all turns to complete a game of Pawns<br>of Loki.', 'reward': r'\{kind=Orb\}'}
        ],
        'template': lambda group: '{{Pawns of Loki Quest|start='+group['startTime']+'}}'
    },
    'Heroes Journey': {
        'sort': 26,
        'avail': 86400,
        'count': 4,
        'quests': [
            {'name': 'Clear Heroes Journey', 'description': 'Clear Heroes Journey.', 'reward': r'\{kind=Orb;count=3\}'}, 
            {'name': 'Clear Heroes Journey', 'description': 'Clear Heroes Journey.', 'reward': r'\{kind=Stamina Potion\}'}
        ],
        'template': lambda group: '{{Heroes Journey Quest|start='+group['startTime']+'}}'
    },
}
ROTATING_QUESTS = {
    "Daily Quest": {
        'sort': 4,
        'groups': [
            {
                'avail': 86400,
                'timeChecker': lambda start,end,i: toTime(start).weekday()==i,
                'quests': [
                    {'name': 'KO Foe', 'description': 'Defeat an enemy.', 'times': 10, 'reward': '\\{kind='+reward1+(';count='+rewardCount if rewardCount != '1' else '')+'\\}'},
                    {'name': 'KO Foe', 'description': 'Defeat an enemy.', 'times': 20, 'reward': '\\{kind='+reward2+(';count='+rewardCount if rewardCount != '1' else '')+'\\}'},
                    {'name': 'Win Arena Duels', 'description': 'Win Arena Duels at any difficulty.', 'times': 2, 'reward': r'\{kind=Hero Feather;count=100\}'}
                ]
            } for reward1, reward2, rewardCount in zip(
                ['Universal Shard','Scarlet Shard','Azure Shard','Verdant Shard','Transparent Shard','Dueling Crest','Stamina Potion'],
                ['Universal Crystal','Scarlet Crystal','Azure Crystal','Verdant Crystal','Transparent Crystal','Orb','Orb'],
                ['1000']*5+['1']*2)
        ],
        'template': lambda groups: '{{Daily Quest|start='+groups[0]['startTime'].split(',')[0]+'|end='+max([group['endTime'].split(',')[-1] for group in groups])+'}}'
    }
}
GROUP_QUESTS = {
    "Tempest Trials": {
        'sort': 16,
        'groups': [
            {
                'avail': 86400,
                'quests': [
                    {'name': 'Clear Trial', 'description': 'Clear one of the final maps in Tempest Trials.', 'reward': r'\{kind=Refining Stone;count=2\}'},
                    {'name': 'Clear Trial', 'description': 'Clear one of the final maps in Tempest Trials.', 'reward': r'\{kind=Stamina Potion\}'},
                    {'name': 'Clear Trial', 'description': 'Clear one of the final maps in Tempest Trials.', 'times': 2, 'reward': r'\{kind=Universal Crystal;count=1500\}'},
                    {'name': 'Clear Trial', 'description': 'Clear one of the final maps in Tempest Trials.', 'times': 3, 'reward': r'\{kind=Hero Feather;count=150\}'}
                ]
            },
            {
                'avail': 86400,
                'quests': [
                    {'name': 'Clear Trial', 'description': 'Clear one of the final maps in Tempest Trials.', 'reward': r'\{kind=Refining Stone;count=4\}'},
                    {'name': 'Clear Trial', 'description': 'Clear one of the final maps in Tempest Trials.', 'reward': r'\{kind=Stamina Potion;count=2\}'},
                    {'name': 'Clear Trial', 'description': 'Clear one of the final maps in Tempest Trials.', 'times': 2, 'reward': r'\{kind=Universal Crystal;count=3000\}'},
                    {'name': 'Clear Trial', 'description': 'Clear one of the final maps in Tempest Trials.', 'times': 3, 'reward': r'\{kind=Hero Feather;count=300\}'}
                ]
            }
        ],
        'template': lambda groups: '{{Tempest Trials Quest|start='+groups[0]['startTime'].split(',')[0]+'|count='+'-'.join([str(int((elapsed(gr['startTime'].split(',')[0],gr['endTime'].split(',')[-1])+1)/86400)) for gr in groups])+'|stage='+groups[0]['quests'][0]['stage']+'}}'
    }
}

def checkSingularQuest(group: dict, obj: dict):
    if group['sort'] != obj['sort']: return False
    # Check availability
    if 'timeChecker' in obj:
        if not all([obj['timeChecker'](start,end) for start,end in zip(group['startTime'].split(','),group['endTime'].split(','))]): return False
    else:
        if 'cycleTime' in group and ('cycle' not in obj or obj['cycle'] != group['cycleTime']): return False
        times = list(zip(group['startTime'].split(','), group['endTime'].split(',')))
        if 'count' in obj:
            if 'availTime' in group:
                if obj['avail'] != group['availTime'] or any([elapsed(start,end)+1 != obj['avail']*obj['count'] for start,end in times]): return False
            elif len(times) % obj['count'] != 0 or any([elapsed(start,end)+1 != obj['avail'] for start,end in times]): return False
        else:
            if 'availTime' in group:
                if obj['avail'] != group['availTime']: return False
            elif any([elapsed(start,end)+1 != obj['avail'] for start,end in times]): return False
    # Check quests
    if len(obj['quests']) != len(group['quests']): return False
    for ref,qu in zip(obj['quests'],group['quests']):
        for key in ref:
            if key not in qu: return False
            if isinstance(ref[key],str):
                if not re.match(re.sub(r'(\s)\s+','\\1',ref[key]),qu[key]): return False
            elif ref[key] != qu[key]: return False
    return True

def checkRotatingQuest(groups: list, ref: dict):
    if not all([gr['sort'] == ref['sort'] for gr in groups]): return False
    if len(groups) > len(ref['groups']): return False
    # Check global availability
    cycle = sum([gr['avail'] for gr in ref['groups']])
    if not all([gr['cycleTime'] == cycle if 'cycleTime' in gr else all((lambda ts: [elapsed(ts[i],ts[i+1]) == cycle for i in range(len(ts)-1)])(gr['startTime'].split(','))) for gr in groups]): return False
    # Search first group in ref
    firstIndex = -1
    for i,refGr in enumerate(ref['groups']):
        # Check availability
        times0 = list(zip(groups[0]['startTime'].split(','),groups[0]['endTime'].split(',')))
        if 'availTime' in groups[0]:
            if refGr['avail'] != groups[0]['availTime']: continue
        elif any([refGr['avail'] != elapsed(start,end)+1 for start,end in times0]): continue
        # Check additional time condition
        if 'timeChecker' in refGr and not all([refGr['timeChecker'](start,end,i) for start,end in times0]): continue
        # Check quests
        if len(refGr['quests']) != len(groups[0]['quests']): continue
        for refQu,qu in zip(refGr['quests'],groups[0]['quests']):
            for key in refQu:
                if key not in qu: firstIndex = -2
                if isinstance(refQu[key],str) and re.match(re.sub(r'(\s)\s+','\\1',refQu[key]),qu[key]): continue
                elif refQu[key] != qu[key]: firstIndex = -2
        if firstIndex == -2: firstIndex = -1; continue
        firstIndex = i; break
    if firstIndex < 0: return False
    # Check remaining group
    for i,gr in enumerate(groups[1:]):
        i = (firstIndex+i+1)%len(ref['groups'])
        if 'availTime' in gr:
            if ref['groups'][i]['avail'] != gr['availTime']: return False
        elif any([ref['groups'][i]['avail'] != elapsed(start,end)+1 for start,end in zip(gr['startTime'].split(','),gr['endTime'].split(','))]): return False
        if 'timeChecker' in ref['groups'][i] and not all([ref['groups'][i]['timeChecker'](start,end,i) for start,end in zip(gr['startTime'].split(','),gr['endTime'].split(','))]): return False
        if len(ref['groups'][i]['quests']) != len(gr['quests']): return False
        for refQu,qu in zip(ref['groups'][i]['quests'],gr['quests']):
            for key in refQu:
                if key not in qu: return False
                if isinstance(refQu[key],str):
                    if not re.match(re.sub(r'(\s)\s+','\\1',refQu[key]),qu[key]): return False
                elif refQu[key] != qu[key]: return False
    return True

def checkGroupQuest(groups, ref):
    if not all([gr['sort'] == ref['sort'] for gr in groups]): return False
    if len(groups) != len(ref['groups']): return False
    for i in range(len(groups)-1):
        if elapsed(groups[i]['endTime'].split(',')[-1], groups[i+1]['startTime'].split(',')[0]) != 1: return False
    for gr,refGr in zip(groups,ref['groups']):
        refGr['sort'] = ref['sort']
        if not checkSingularQuest(gr, refGr): return False
    return True

def templateMissions(groups):
    for i,group in enumerate(groups):
        for pattern, obj in SINGULAR_QUESTS.items():
            if not (re.match(pattern, group['title']) and checkSingularQuest(group, obj)): continue
            groups[i] = obj['template'](group)
            break

    groupedGroup = {}
    for i,group in enumerate(groups):
        if isinstance(group, str): continue
        if group['title'] in groupedGroup:
            groupedGroup[group['title']]['groups'].append(group)
            groupedGroup[group['title']]['index'].append(i)
        else:
            groupedGroup[group['title']] = {'groups': [group], 'index': [i]}
    removeIndex = []
    for title,group in groupedGroup.items():
        group['groups'] = sorted(group['groups'], key=lambda o:o['startTime'])
        for pattern, obj in ROTATING_QUESTS.items():
            if not (re.match(pattern, title) and checkRotatingQuest(group['groups'], obj)): continue
            # Replace the groups
            i = min(group['index'])
            group['index'].remove(i)
            removeIndex += group['index']
            groups[i] = obj['template'](group['groups'])
            break
        if group['index'][0] in removeIndex: continue
        for pattern, obj in GROUP_QUESTS.items():
            if not (re.match(pattern, title) and checkGroupQuest(group['groups'], obj)): continue
            i = min(group['index'])
            group['index'].remove(i)
            removeIndex += group['index']
            groups[i] = obj['template'](group['groups'])
            break
    for i in sorted(removeIndex, reverse=True): groups.pop(i)
    return groups