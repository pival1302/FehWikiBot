#! /usr/bin/env python3

import re
from num2words import num2words

from globals import DIFFICULTIES, MOVE_TYPE
import util
import mapUtil
import wikiUtil

def getDuplicateMap(mapId: str):
    result = wikiUtil.request_get({
        "action": "query",
        "titles": "File:Map "+mapId+".webp",
        "prop": "duplicatefiles",    
    })
    if 'error' in result:
        print(util.ERROR + f"Failed to get duplicates of \"{mapId}\": {result['error']['info']}")
    elif 'query' in result:
        result = list(result['query']['pages'].values())[0]
        duplicates = [p['name'] for p in (result['duplicatefiles'] if 'duplicatefiles' in result else []) if p['name'][:5] == 'Map_O']
        if len(duplicates) == 0:
            print(util.ERROR + f"No duplicates for \"{mapId}\"")
        else:
            return duplicates[0][4:9]
    else:
        print(util.ERROR + f'{result}')

def GCMapLayout(map_id):
    GC = int((int(map_id[1:])-1) / 30) + 1
    page = wikiUtil.getPageContent(f"Grand Conquests {GC}")[f"Grand Conquests {GC}"]
    GCLayout = re.search(r"{{MapLayout\D+"+map_id+".*\n(?:(?!}}\n)(?:.*\n))+}}", page)
    if not GCLayout:
        return
    else:
        return GCLayout[0]

def RDMapLayout(mapId: str, GCmapId: str):
    if not GCmapId:
        backdrop = ''
        mapObjects = [ ['']*8 ]*10
    else:
        GCLayout = GCMapLayout(GCmapId)
        backdrop = re.search(r"backdrop\s*=\s*(\w*)", GCLayout)[1]
        mapObjects = [ re.findall("[a-h]"+str(i)+r"=\s*(\{\{.+?\}\}|)\s*(?:\n|\|\s*(?=[a-h]\d))", GCLayout) for i in range(10,0,-1) ]

    reEnemyCamp = re.compile(r"\{\{RDTerrain\|color=Enemy\|type=Camp(?: Spawn)?\}\}")
    reEnemySpawn = re.compile(r"\{\{RDTerrain\|color=Enemy\|type=Spawn\}\}")
    reEnemyWarpSpawn = re.compile(r"\{\{RDTerrain\|color=Enemy\|type=Warp Spawn\}\}")
    cellIsKind = lambda a, b, regex: a >= 0 and b >= 0 and a < len(mapObjects) and b < len(mapObjects[a]) and regex.search(mapObjects[a][b])

    if not GCmapId:
        mapNormal = mapObjects
    # 2 simple spawns.
    elif sum([sum([1 for cell in line if reEnemySpawn.search(cell)]) for line in mapObjects]) == 2:
        mapNormal = [[reEnemySpawn.sub("", cell) for cell in line] for line in mapObjects]
    # 2 camps.
    elif sum([sum([1 for cell in line if reEnemyCamp.search(cell)]) for line in mapObjects]) == 2:
        cond = lambda i, j: cellIsKind(i, j, reEnemyWarpSpawn) and any([cellIsKind(i+x, j+y, reEnemyCamp) for x,y in [(0,-1),(0,1),(-1,0),(1,0)]])
        mapNormal = [[re.sub("Warp Spawn", "Warp", cell) if cond(i,j) else cell for j,cell in enumerate(line)] for i,line in enumerate(mapObjects)]
    # 6 warp spawns.
    # 3 around fortress + 3 around camp. The two opposite around camp are removed.
    elif sum([sum([1 for cell in line if reEnemyWarpSpawn.search(cell)]) for line in mapObjects]) == 6:
        cond = lambda i, j: cellIsKind(i, j, reEnemyWarpSpawn) and any([cellIsKind(i+x, j+y, reEnemyCamp) and cellIsKind(i+x*2, j+y*2, reEnemyWarpSpawn) for x,y in [(0,-1),(0,1),(-1,0),(1,0)]])
        mapNormal = [[re.sub("Warp Spawn", "Warp", cell) if cond(i,j) else cell for j,cell in enumerate(line)] for i,line in enumerate(mapObjects)]
    else:
        print(util.TODO + "RD map with unknow pattern")
        mapNormal = [[''] * len(mapObjects[0])] * len(mapObjects)

    key = [[ c + str(n) for c in ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h'] ] for n in range(10, 0, -1) ]
    content = "|version1=Standard\n|mapImage={{MapLayout|type=RD|baseMap=" + mapId + "|backdrop=" + backdrop + "\n"
    for lKey,lMap in zip(key,mapNormal):
        for cKey,cMap in zip(lKey,lMap):
            content += "| " + cKey + "=" + cMap + ' '
        content = content[:-1] + '\n'
    content += "}}\n"
    content += "|version2=Infernal\n|mapImageV2={{MapLayout|type=RD|baseMap=" + mapId + "|backdrop=" + backdrop + "\n"
    for lKey,lMap in zip(key,mapObjects):
        for cKey,cMap in zip(lKey,lMap):
            content += "| " + cKey + "=" + cMap + ' '
        content = content[:-1] + '\n'
    content += "}}"
    return content

def RDMapInfobox(StageEvent: dict, GCmapId):
    info = {
        'id_tag': 'OCCUPATION',
        'banner': 'Banner_Rival_Domains_' + MOVE_TYPE[int(StageEvent['banner_id'][-1])-1] + '.png',
        'group': 'Rival Domains',
        'requirement': 'Reach the target [[Rival Domains#Scoring|score]] to earn<br>a reward. Bonus for defeating<br>foes with {{Mt|'+MOVE_TYPE[int(StageEvent['banner_id'][-1])-1].lower()+'}} allies.',
        'mode': 'Reinforcement Map',
        'lvl': {'Normal': 30, 'Hard': 35, 'Lunatic': 40, 'Infernal': 40},
        'rarity': {'Normal': 3, 'Hard': 4, 'Lunatic': 5, 'Infernal': 5},
        'stam': {}, 'reward': {}, 'map': {}
    }

    for index in range(len(StageEvent['scenarios'])):
        diff = DIFFICULTIES[StageEvent['scenarios'][index]['difficulty']]
        info['stam'].update({diff: StageEvent['scenarios'][index]['stamina']})
        info['reward'].update({diff: StageEvent['scenarios'][index]['reward']})

    return re.sub(r"\|mapImage[^}]+\}\}", RDMapLayout(StageEvent['id_tag'], GCmapId), mapUtil.MapInfobox(info))

def RivalDomains(mapId: str):
    StageEvent = util.fetchFehData("Common/SRPG/StageEvent")[mapId]

    dupId = getDuplicateMap(mapId)
    content = RDMapInfobox(StageEvent, dupId) + '\n'
    content += mapUtil.MapAvailability(StageEvent['avail'])
    content += "==Unit data==\n===Enemy AI Settings===\n{{EnemyAI|activeall}}\n===Stats===\n{{RivalDomainsEnemyStats}}\n"
    content += "===List of enemy units===\nThese enemy units make up the brigade for this rival domains:\n{{RDEnemyBrigade|}}\n"
    content += "==Strategy / General Tips==\n*\n"
    content += "==Trivia==\n*\n"
    if dupId:
        GC = int((int(dupId[1:])-1) / 30) + 1
        page = wikiUtil.getPageContent(f"Grand Conquests {GC}")[f"Grand Conquests {GC}"]
        area = re.search(r"\|\s*(\d+)\s*\n\|\s*\{\{MapLayout\D+"+dupId, page)[1]
        content = content[:-1] + " This map layout is the same as Area "+area+" of [[Grand Conquests "+str(GC)+"|the "+num2words(GC, to="ordinal")+" Grand Conquests event]].\n"
    content += "{{Special Maps Navbox}}"
    return content

from sys import argv

if __name__ == "__main__":
    for arg in argv[1:]:
        if arg[0] == 'Q':
            print(RivalDomains(arg))