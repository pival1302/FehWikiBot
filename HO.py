#! /usr/bin/env python3

from datetime import datetime

import util
import mapUtil
from globals import DATA
from scenario import getSound

SERIES_BGM = util.fetchFehData('Common/SRPG/StageBgm/HeroTrial', 'origin')

def getHeroJson(heroId: int):
    HERO = util.fetchFehData("Common/SRPG/Person", False)

    for hero in HERO:
        if hero['id_num'] == heroId:
            return hero
    return {}

def HeroicOrdeals(mapId: str):
    heroId = int(mapId[1:])
    hero = getHeroJson(heroId)
    SRPGMap = util.readFehData("Common/SRPGMap/" + mapId + ".json")
    diff = heroId < 191 and 'Normal' or heroId < 317 and 'Hard' or 'Lunatic'
    release = util.cargoQuery("Units", "ReleaseDate", "IntID="+str(heroId), limit=1) or datetime.now().strftime("%Y-%m-%d")
    if isinstance(release,dict): release = release['ReleaseDate']

    SRPGMap['field'].update({'player_pos': SRPGMap['player_pos']})

    content = "{{HeroPage Tabs/Heroic Ordeals}}"
    content += mapUtil.MapInfobox({
        'title': DATA["MID_STAGE_SELECT_HERO_TRIAL"],
        'name': DATA["M"+hero['id_tag']],
        'group': 'Heroic Ordeals',
        'map': SRPGMap['field'],
        'lvl': {diff: (heroId < 191 and 30 or heroId < 317 and 35 or 40)},
        'rarity': {diff: (heroId < 191 and 4 or 5)},
        'stam': {diff: 0},
        'reward': {diff: [{"kind": 30, "move_type": hero['move_type'], "count": (heroId < 191 and 2 or heroId < 317 and 8 or 40)}]},
        'requirement': "The ordeal challenger must<br>defeat at least 2 foes.<br>All allies must survive.<br>Turns to win: 20",
        'bgms': (getSound(SERIES_BGM[str(hero['series'])]['map_bgm'])+'.ogg', getSound(SERIES_BGM[str(hero['series'])]['battle_bgm'])+'.ogg')
    }) + "\n"
    content += mapUtil.MapAvailability({ 'start': ((release + 'T07:00:00Z') if release else None) })
    content += "==Unit data==\n{{#invoke:UnitData|main\n|" + diff + "=" + mapUtil.UnitData(SRPGMap) + "\n}}\n"
    content += mapUtil.InOtherLanguage(["MID_STAGE_SELECT_HERO_TRIAL", "M" + hero['id_tag']], "a", False)
    content += "{{Heroic Ordeals Navbox}}"

    return content

from sys import argv

if __name__ == "__main__":
    print(HeroicOrdeals(argv[1]))