#! /usr/bin/env python3

import re
from datetime import datetime

import util
from globals import UNIT_IMAGE, SOUNDS, TIME_FORMAT

DUO_NAMES = { res['Page']: res for res in util.cargoQuery('Units=U,Languages=L,' +\
            'DuoHero=DU,CharacterSortValues=DCSV,Languages=DL,' +\
                        'CharacterSortValues=TCSV,Languages=TL,' +\
            'HarmonizedHero=HU,CharacterSortValues=HCSV,Languages=HL',
        fields='U._pageName=Page,U.Name=NameEN,IFNULL(DL.English,HL.English)=DuoEN,TL.English=TrioEN,' +\
            'L.Japanese=NameJP,IFNULL(DL.Japanese,HL.Japanese)=DuoJP,TL.Japanese=TrioJP',
        where= '(DU._pageName IS NOT NULL OR HU._pageName IS NOT NULL) AND ' +\
            "CONCAT(U._pageName,'/Misc')=L._pageName AND " +\
            "(DL._pageName IS NULL OR DCSV.name LIKE CONCAT(DL.English,'%')) AND " +\
            "(TL._pageName IS NULL OR TCSV.name LIKE CONCAT(TL.English,'%')) AND " +\
            "(HL._pageName IS NULL OR HCSV.name LIKE CONCAT(HL.English,'%'))",
        join = 'U._pageName=DU._pageName,U._pageName=HU._pageName,' +\
            'U._pageNamespace=L._pageNamespace,' +\
            'DU.WikiSecondPerson=DCSV.name,DCSV._pageName=DL._pageName,' +\
            'DU.WikiThirdPerson=TCSV.name,TCSV._pageName=TL._pageName,' +\
            'HU.WikiSecondPerson=HCSV.name,HCSV._pageName=HL._pageName',
        group = 'U._pageName') }
for k,v in DUO_NAMES.items(): DUO_NAMES[k]['NameJP'] = re.search('[^　]+$', v['NameJP'])[0]
DATA_JP = {r["key"]: r["value"] for r in util.fetchFehData("JPJA/Message/Data")}

def getSound(bgmId):
    if bgmId in SOUNDS:
        return SOUNDS[bgmId]['list'][0]['file']
    else:
        return bgmId

def getColor(R,G,B,A=255):
    return '#' + \
        (hex(int(R)).upper() + '0')[2:4] + \
        (hex(int(G)).upper() + '0')[2:4] + \
        (hex(int(B)).upper() + '0')[2:4]

def exportStructure(objs):
    def hashToString(hash):
        s = []
        for k2 in ['background','music','sound','transition','alpha','time','in','out',
                'unit','image','expression','duo','trio','name','nameJP','noswap','color','text','textJP']:
            if k2 in hash:
                s.append(f"{k2}={hash[k2]}")
        return '{' + ';'.join(s) + '};'

    for k,convs in list(objs.items()):
        ret = []
        prev = None
        for i,conv in enumerate(convs):
            o = conv.copy()
            if 'transition' in o:
                if 'out' in o and o['in'] == o['out']:
                    o['time'] = o['in']
                    o.pop('in'); o.pop('out')
                if o['alpha'] == '255': o.pop('alpha')
            elif 'unit' in o:
                unit = UNIT_IMAGE[o['unit']] if o['unit'] in UNIT_IMAGE else None
                name = (unit['name'] if 'name' in unit else util.getName(unit['id_tag'])) if unit else None
                nameEN = (name[:name.find(':')] if name.find(':') != -1 else name) if name else None
                nameJP = (unit['nameJP'] if 'nameJP' in unit else DATA_JP['M'+unit['id_tag']] if 'M'+unit['id_tag'] in DATA_JP else None) if unit else None
                if not unit and o['unit'] != '':
                    print(util.TODO + f"Unknow unit '{o['unit']}'")

                # Unit handling
                if prev and o['unit'] == prev['unit']:
                    # Prev fallback
                    o.pop('unit')
                    o.pop('noswap','')
                    if o['expression'] == prev['expression']: o.pop('expression')
                    else: o['expression'] = o['expression'][5:]
                    if o['name'] == prev['name']:
                        o.pop('name'); o.pop('nameJP')
                    elif nameEN and o['name'] == nameEN: o['name'],o['nameJP'] = '',''
                    # Duo/Harmonized
                    if 'name' in o and 'legendary' in (unit or {}) and 'kind' in (unit['legendary'] or {}) and unit['legendary']['kind'] in [2,3]:
                        if o['name'] == '':
                            o['duo'] = ''; o.pop('name'); o.pop('nameJP')
                        elif not name or name not in DUO_NAMES: o['duo'] = 1
                        elif o['name'] == DUO_NAMES[name]['DuoEN']:
                            o['duo'] = 1; o.pop('name'); o.pop('nameJP')
                        elif o['name'] == DUO_NAMES[name]['TrioEN']:
                            o['trio'] = 1; o.pop('name'); o.pop('nameJP')
                        else: o['duo'] = 1
                else:
                    # New unit
                    o['unit'] = name if name != None else ('<!--' + o['unit'] + '-->') if o['unit'] != '' else ''
                    if o['expression'] == 'Face': o.pop('expression')
                    else: o['expression'] = o['expression'][5:]
                    if nameJP and o['nameJP'] == nameJP: o.pop('nameJP')
                    if nameEN and o['name'] == nameEN: o.pop('name')
                    # Duo/Harmonized
                    elif unit and 'legendary' in unit and 'kind' in (unit['legendary'] or {}) and unit['legendary']['kind'] in [2,3]:
                        if not name or not name in DUO_NAMES: o['duo'] = 1
                        elif o['name'] == DUO_NAMES[name]['DuoEN']:
                            o['duo'] = 1; o.pop('name'); o.pop('nameJP')
                        elif o['name'] == DUO_NAMES[name]['TrioEN']:
                            o['trio'] = 1; o.pop('name'); o.pop('nameJP')
                        else: o['duo'] = 1
                prev = conv
    
                # Text
                if 0 in o['color'] and o['color'][0] != '#FFFFFF' and all([i in o['color'] and o['color'][i] == o['color'][0] for i in range(len(o['text']))]) and all([i in o['colorJP'] and o['colorJP'][i] == o['color'][0] for i in range(len(o['textJP']))]):
                    o['color'] = o['color'][0]
                else:
                    for j,c in o['color'].items():
                        if c == '#FFFFFF' or j >= len(o['text']): continue
                        o['text'][j] = f"$c:{c}:{o['text'][j]}$"
                    for j,c in o['colorJP'].items():
                        if c == '#FFFFFF' or j >= len(o['textJP']): continue
                        o['textJP'][j] = f"$c:{c}:{o['textJP'][j]}$"
                    o.pop('color')
                o.pop('colorJP')
                tEN = o.pop('text'); tJP = o.pop('textJP')
                while len(tEN) > 0 and len(tJP) > 0:
                    o['text'] = tEN.pop(0); o['textJP'] = tJP.pop(0)
                    ret.append(hashToString(o))
                    o = {'color': o['color']} if 'color' in o else {}
                if len(tEN) > 0: o['text'] = '\n\t'.join(tEN)
                elif len(tJP) > 0: o['textJP'] = '\n\t'.join(tJP)
                else: continue

            ret.append(hashToString(o))
        objs[k] = '[\n  ' + '\n  '.join(ret) + '\n]'

    return objs

def _normalizeBlocks(enData, jpData):
    enData = enData.replace('$Nu', '$Summoner$').replace('$Nf', '$Friend$').replace(';','$,$').replace('\n','\\n')
    jpData = jpData.replace('$Nu', '$Summoner$').replace('$Nf', '$Friend$').replace(';','$,$').replace('\n','\\n')
    blocksEN = re.findall(r'[$|](?:\$(?:Summoner|Friend|,)\$|[^$|]+)*', enData)
    blocksJP = re.findall(r'[$|](?:\$(?:Summoner|Friend|,)\$|[^$|]+)*', jpData)
    if jpData == '': return blocksEN,['']*len(blocksEN)
    i = 0

    while True:
        if i >= max(len(blocksEN),len(blocksJP)): break
        if i >= len(blocksEN):
            if blocksJP[i] in ('$k','|') or blocksJP[i][:2] == '$p': blocksEN.insert(i, blocksJP[i][:2])
            else: print(util.ERROR + 'Fail to combine EN and JP. Remaining', blocksJP[i:])
        elif i >= len(blocksJP):
            if blocksEN[i] in ('$k','|') or blocksEN[i][:2] == '$p': blocksJP.insert(i, blocksEN[i][:2])
            else: print(util.ERROR + 'Fail to combine EN and JP. Remaining', blocksEN[i:])
        elif (blocksEN[i] == '$k') ^ (blocksJP[i] == '$k'):
            if blocksEN[i] == '$k': blocksJP.insert(i, '$k')
            else: blocksEN.insert(i, '$k')
        elif (blocksEN[i][:2] == '$p') ^ (blocksJP[i][:2] == '$p'):
            if blocksEN[i][:2] == '$p': blocksJP.insert(i, '$p')
            else: blocksEN.insert(i, '$p')
        elif (blocksEN[i][:2] == '$c') ^ (blocksJP[i][:2] == '$c'):
            if blocksEN[i][:2] == '$c': blocksJP.insert(i, '$c255,255,255,255')
            else: blocksEN.insert(i, '$c255,255,255,255')
        elif (blocksEN[i][0] == '|') ^ (blocksJP[i][0] == '|'):
            if blocksEN[i][0] == '|': blocksJP.insert(i, '|')
            else: blocksEN.insert(i, '|')
        elif blocksEN[i][0] == '$' and blocksEN[i][:2] != '$p' and blocksEN[i] != blocksJP[i]:
            print(util.ERROR + 'Fail to combine EN and JP at index', i)
            blocksJP = blocksJP[:i] + [re.sub(r'^(\||\$p).*$','\\1',block,re.DOTALL) for block in blocksEN[i:]] + blocksJP[i:]
            blocksEN += [re.sub(r'^(\||\$p).*$','\\1',block,re.DOTALL) for block in blocksJP[len(blocksEN):]]
            break
        i += 1
    return blocksEN,blocksJP

def _parseSingle(enData: str, jpData: str):
    convs = []
    conv = {'text': [],'textJP': []}
    def pushConv(noswap: bool):
        if len(conv['text']) > 0 or len(conv['textJP']) > 0: convs.append(conv.copy())
        conv['text'] = []
        conv['textJP'] = []
        conv['color'] = {}
        conv['colorJP'] = {}
        if noswap: conv['noswap'] = 1

    for blockEN,blockJP in zip(*_normalizeBlocks(enData,jpData)):
        if blockEN[:2] == '$b': # Background image
            pushConv(True)
            convs.append({'background': blockEN[2:]})
        elif blockEN[:4] == '$Sbp': # Start music
            pushConv(True)
            convs.append({'music': getSound(blockEN[4:blockEN.find(',')])})
        elif blockEN[:4] == '$Sbs': pass # Stop music
        elif blockEN[:4] == '$Sbv': pass # Change music volume
        elif blockEN[:4] == '$Ssp': # Sound effect
            pushConv(True)
            convs.append({'sound': getSound(blockEN[4:])})
        elif blockEN[:3] == '$Fo': # Fade panel show
            pushConv(True)
            time,R,G,B,A = blockEN[3:].split(',')
            convs.append({'transition': getColor(R,G,B), 'alpha': A, 'in': time})
        elif blockEN[:3] == '$Fi': # Fade panel hide
            if 'transition' in convs[-1]:
                convs[-1]['out'] = blockEN[3:]
            elif ('background' in convs[-1] or 'music' in convs[-1] or 'sound' in convs[-1]) and 'transition' in convs[-2]:
                convs[-2]['out'] = blockEN[3:]
            else:
                print(util.ERROR + 'Unexpected $Fi flag')
        elif blockEN[:2] == '$w': pass # TODO
        elif blockEN == '$k': pass # Tap screen
        elif blockEN[:2] == '$p': # Clear text
            if blockEN[2:] != '': conv['text'].append(blockEN[2:])
            if blockJP[2:] != '': conv['textJP'].append(blockJP[2:])
        elif blockEN[:3] == '$Wm': # Unit's name, face, expr
            if len(conv['text']) > 0 or len(conv['textJP']): convs.append(conv)
            conv = {'text':[],'color':{},'textJP':[],'colorJP':{}}
            name,unit,expr = blockEN[3:].split(',')
            conv['unit'] = unit
            conv['name'] = util.getName(name, False)
            conv['nameJP'] = DATA_JP[name] if name in DATA_JP else ('<!--'+name+'-->')
            conv['expression'] = expr
        elif blockEN[:2] == '$n': # Unit's name
            pushConv(False)
            conv['name'] = util.getName(blockEN[2:], False)
            conv['nameJP'] = DATA_JP[blockEN[2:]] if blockEN[2:] in DATA_JP else blockEN[2:]
        elif blockEN[:2] == '$E': # Unit's expr
            pushConv(False)
            conv['expression'] = blockEN[2:]
        elif blockEN[0] == '|':
            if blockEN[1:] != '': conv['text'].append(blockEN[1:])
            if blockJP[1:] != '': conv['textJP'].append(blockJP[1:])
        elif blockEN[:2] == '$c':
            R,G,B,A = blockEN[2:].split(',')
            conv['color'][len(conv['text'])] = getColor(R,G,B,A)
            R,G,B,A = blockJP[2:].split(',')
            conv['colorJP'][len(conv['textJP'])] = getColor(R,G,B,A)
        else:
            print(util.TODO + str((blockEN,blockJP)))
    convs.append(conv)
    return convs

def _parseStructure(objEn, objJp=None):
    convs = {}
    for k in list(objEn.keys()):
        if k[-4:] == '_BGM' or k[-6:] == '_IMAGE': continue
        elif k[:13] == 'MID_SCENARIO_':
            convs[k[13:]] = _parseSingle(objEn.pop(k), objJp.pop(k) if objJp and k in objJp else '')
        else:
            convs[k] = _parseSingle(objEn.pop(k), objJp.pop(k) if objJp and k in objJp else '')
    for k,v in objEn.items():
        if k[-4:] == '_BGM':
            o = {'music': getSound(v)}
            if k[13:-4] in convs: convs[k[13:-4]].insert(0, o)
            elif k[:-4] in convs: convs[k[:-4]].insert(0, o)
            else: convs[k[:-4]] = [o]
        else:
            o = {'background': v}
            if k[13:-6] in convs: convs[k[13:-6]].insert(0, o)
            elif k[:-6] in convs: convs[k[:-6]].insert(0, o)
            else: convs[k[:-6]] = [o]
    return convs

def parseScenario(mapId: str, useJP=True):
    enJSON = {a['key']: a['value'] for a in util.readFehData("USEN/Message/Scenario/" + mapId + ".json")}
    jaJSON = useJP and {a['key']: a['value'] for a in util.readFehData("JPJA/Message/Scenario/" + mapId + ".json")}
    convs = _parseStructure(enJSON, jaJSON)
    return exportStructure(convs)

def StoryNavBar(mapId, prev=None, next=None):
    pmapid = mapId[:-1] + str(int(mapId[-1]) -1) if mapId[0] in ['S','X'] else None
    pmapid = prev if prev != None else util.getName(pmapid) if util.getName(pmapid) != pmapid else ''
    nmapid = mapId[:-1] + str(int(mapId[-1]) +1) if mapId[0] in ['S','X'] else None
    nmapid = next if next != None else util.getName(nmapid) if util.getName(nmapid) != nmapid else ''
    #print(util.TODO + f'Navbar {mapId} ({pmapid},{nmapid})')
    return '{{Story Navbar|' + pmapid + '|' + nmapid + '}}'

def ScenarioNavbar(category, start=None, title=None):
    if category == 'paralogue':
        start = datetime.strptime(start, TIME_FORMAT)
        if (start.month == 12 and start.day > 25) or (start.month == 1 and start.day <= 5): category = 'New Year Festival'
        elif start.month == 2: category = 'Day of Devotion'
        elif start.month == 3: category = 'Spring Festival'
        elif start.month == 4: category = 'Child Heroes'
        elif start.month == 5: category = 'Bridal Festival'
        elif start.month in (6,7): category = 'Summer Vacation'
        elif start.month == 10: category = 'Harvest Festival'
        elif start.month == 12 and start.day <= 25: category = 'Winter Festival'
        else:
            category = util.askFor(intro=f'To which category belongs "{title or ""}"?') or ''
            prev,next = '',''
            if category != '':
                prev = util.askFor(intro=f"Is there a previous paralogue?") or ''
                next = util.askFor(intro=f"A next paralogue?") or ''
            category += (f"|prev={prev}" if prev != '' else '') + (f"|next={next}" if next != '' else '')
        return '{{Scenario Navbar|paralogue|'+category+'}}'
    else:
        return '{{Scenario Navbar|'+category+'}}'

def ScenarioNavbox(category):
    return '{{Scenario Navbox|'+category+'}}'

def Conversation(mapId, tag):
    o = parseScenario(mapId)
    if tag[:13] == 'MID_SCENARIO_' and tag[13:] in o:
        o = o[tag[13:]]
    elif tag in o:
        o = o[tag]
    else:
        print(util.ERROR + f"No key {tag} on scenario {mapId}")
        o = ''
    return '{{#invoke:Scenario|scenario|1=' + o + '}}'

def Story(mapId: str):
    STORY_KEYS = ['OPENING','MAP_BEGIN','MAP_END','ENDING']
    convs = parseScenario(mapId)

    others = [k for k in convs.keys() if k not in STORY_KEYS]
    if len(others) != 0:
        print(util.TODO + f"Other keys on scenario {mapId}: {others}")

    s = '{{#invoke:Scenario|story\n'
    for k in STORY_KEYS:
        if k in convs:
            s += f"|{k.lower().replace('_',' ')}={convs[k]}\n"
    s += '}}'
    return s


if __name__ == "__main__":
    from sys import argv
    for arg in argv[1:]:
        o = parseScenario(arg)
        for k,v in o.items():
            print(k, v)

"""
{{#invoke:Scenario|tapBattle|floor20=[]|floor40=[]|floor60=[]|floor80=[]|floor100=[]}}
"""