#! /usr/bin/env python3

import re
from datetime import datetime
#from os.path import exists

from globals import DATA, UNITS, MOVE_TYPE, WEAPON_TYPE
import util
from mapUtil import InOtherLanguage
from scenario import DATA_JP
#from Reverse import reverseCaptainSkill

def EnemyInfobox(unit):
    derivedTag = lambda s: 'M' + unit['id_tag'][:4] + s + unit['id_tag'][3:]
    artist = re.search(r'\((.+)\)$',util.getName(derivedTag('ILLUST')))

    s = '{{Enemy Infobox\n'
    s += '|Name=' + util.getName(unit['id_tag'], False) + '\n'
    s += '|Title=' + util.getName(derivedTag('HONOR')) + '\n'
    s += '|Origin=Fire Emblem Heroes\n'
    s += '|Person=\n'
    s += '|Gender=' + unit['face_name'][unit['face_name'].find('_',8)+1:unit['face_name'].rfind('_')] + '\n'
    s += '|WeaponType=' + WEAPON_TYPE[unit['weapon_type']] + '\n'
    s += '|MoveType=' + MOVE_TYPE[unit['move_type']] + '\n'
    s += '|actorEN=' + util.getName(derivedTag('VOICE')) + '\n'
    s += '|actorJP=' + (DATA_JP[derivedTag('VOICE')] if derivedTag('VOICE') in DATA_JP else '') + '\n'
    s += '|artist=' + (artist[1] if artist else util.getName(derivedTag('ILLUST'))) + '\n'
    s += '|additionDate=' + datetime.now().strftime('%Y-%m-%d') + '\n'
    s += '|Properties=\n'
    s += '|description=' + util.getName(derivedTag('H')).replace('\n',' ') + '\n'
    s += '|TagID=' + unit['id_tag'][4:] + '\n'
    s += f"|InternalID={unit['id_num']}\n"
    s += '}}'
    return s

def UnitStats(stats, growth):
    s = "{{Stats Page\n"
    s += ''.join([f"|Lv1{k.upper()}={v+1}" for k,v in stats.items()]) + '\n'
    s += ''.join([f"|GR{k.upper()}={v}" for k,v in growth.items()]) + '\n'
    s += '}}'
    return s

def UnitSkills(skills, isEnemy):
    s = '==Skills==\n'
    s += '===Weapons===\n{{Weapons Table'
    if isEnemy and skills[0]:
        s += '/enemy\n' + f"|weapon4={util.getName(skills[0])}\n"
    s += '}}\n'
    s += '===Assists===\n{{Assists Table'
    if isEnemy and skills[1]:
        s += '\n' + ''.join([f"|assist{i+1}={util.getName(sk)}\n" for i,sk in enumerate(skills[1:3]) if sk])
    s += '}}\n'
    s += '===Specials===\n{{Specials Table'
    if isEnemy and skills[3]:
        s += '\n' + f"|special1={util.getName(skills[3])}\n"
    s += '}}\n'
    s += '===Passives===\n{{Passives Table'
    if isEnemy and None:
        s += '\n' #+ f"|special1={util.getName(skills[3])}\n"
    s += '}}'
    return s

def Quotes(hero_id):
    QUOTE_DEFINITIONS = [("Summoning", "JOIN", 1),
                         ("Castle", "HOME", 5),
                         ("Friend greeting", "FRIEND", 1),
                         ("Leveling up", "LEVEL", 3),
                         ("Ally growth", "SKILL", 1),
                         ("5★ LV. 40 conversation", "STRONGEST", 1)]
    VOICE_DEFINITIONS = [("Attack", "ATTACK", 1, 2),
                          ("Damage", "DAMAGE", 3, 2),
                          ("Special trigger", "SKILL", 5, 4),
                          ("Defeat", "DEAD", 9, 1),
                          ("Status page", "STATUS", 10, 8),
                          ("Turn action", "MAP", 18, 3)]
    romanized = UNITS[hero_id]['roman']
    EN = {o['key']: o['value'] for o in util.readFehData("USEN/Message/Character/"+romanized+".json")}
    JP = {o['key']: o['value'] for o in util.readFehData("JPJA/Message/Character/"+romanized+".json")}

    s = '{{HeroPage Tabs}}\n'
    if hero_id[0] == 'E': s += '<!--\n'
    for (title, tag, count) in QUOTE_DEFINITIONS:
        s += '=='+title+'==\n'
        for i in range(1,count+1):
            k = f"MID_{romanized}_{tag}" + (str(i) if count != 1 else '')
            en = EN.pop(k,'').replace('$k$p','<br>').replace('\\n',' ').replace('$Nf','{{Friend}}').replace('$Nu', '{{Summoner}}')
            jp = JP.pop(k,'').replace('$k$p','<br>').replace('\\n',' ').replace('$Nf','{{Friend}}').replace('$Nu', '{{Summoner}}')
            if tag == 'LEVEL': s += f'===+[{(count-i)*2},{(count-i)*2+1}] points===\n'
            s += '{{bq|'+en+'}}\n{{bq|'+jp+'|ja}}\n{{Clear}}\n'
    if hero_id[0] == 'E': s += '-->\n'

    for (title, tag, init, count) in VOICE_DEFINITIONS:
        template = 'Audio' if tag != 'STATUS' else 'Status'
        if tag == 'STATUS' and hero_id[0] == 'E': s += '<!--\n'
        s += '=='+title+'==\n'
        s += '{{'+template+'TableHeader}}\n'
        for i in range(count):
            k = f"MID_{romanized}_VOICE{init+i:02}"
            en = EN.pop(k,'').replace('$k$p','<br>').replace('\\n',' ').replace('$Nf','{{Friend}}').replace('$Nu', '{{Summoner}}')
            s += '{{'+template+'TableRow|VOICE_{{MF|1={{BASEPAGENAME}}}}_'+f"{tag}_{i+1}.wav|{en if en != '-' and re.search(r'[a-zA-Z]',en) else ''}"+'}}\n'
            if tag == 'STATUS': s = s[:-3] + f"|{1 if i < 4 else 4 if i < 6 else 5}" + '}}\n'
        s += '|}\n{{'+template+'TableHeader|ja}}\n'
        for i in range(count):
            k = f"MID_{romanized}_VOICE{init+i:02}"
            jp = JP.pop(k,'').replace('$k$p','<br>').replace('\\n',' ').replace('$Nf','{{Friend}}').replace('$Nu', '{{Summoner}}')
            s += '{{'+template+'TableRow|VOICE_{{MF|1={{BASEPAGENAME}}}}_'+f"{tag}_{i+1}_jp.wav|{jp}|ja}}}}\n"
            if tag == 'STATUS': s = s[:-5] + f"{1 if i < 4 else 4 if i < 6 else 5}|" + 'ja}}\n'
        s += '|}\n{{Clear}}\n'
    if hero_id[0] == 'E': s += '-->\n'
    s += '{{StoryAppearances}}'

    if len(EN) != 0:
        print(util.TODO + f'Unused quotes: {EN}')
    return s

def Misc(hero_id):
    from MiniUnit import getSpriteSheets, getSprites
    name = util.getName(hero_id)
    cName = util.cleanStr(name)

    s = '{{HeroPage Tabs}}\n'
    s += '{{MapAppearances}}\n'
    s += '==Availability==\n{{GeneralSummonRarities}}\n{{HeroFocusList}}\n{{DistributedAvailability}}\n'
    s += '{{HeroBonusList}}\n'
    s += '{{EventAppearances}}\n'
    
    tips = [k for k in DATA.keys() if re.match(r'MID_TIPS_Summon_\w+_'+hero_id[4:],k)]
    if len(tips) > 0:
        s += '{{Hero Tips\n'
        s += '|textUSEN=' +DATA[tips[0]].replace('\n',' ') + '\n'
        s += '|textJPJA=' + (DATA_JP[tips[0]] if tips[0] in DATA_JP else '').replace('\n',' ') + '\n'
        s += '}}\n'

    s += '==Trivia==\n* \n'
    s += InOtherLanguage(['M'+hero_id, 'M'+hero_id[:4]+'HONOR'+hero_id[3:]])

    s += '==Gallery==\n<gallery>\n'
    s += f"{cName} BtlFace BU.webp\n"
    s += f"{cName} BtlFace BU D.webp\n"
    for i in range(4):
        s += f"{cName} pop0{i+1}.png\n"
    s += '</gallery>\n'
    s += '===Sprites===\n<gallery>\n'
    for f in getSprites(UNITS[hero_id]):
        s += f[5:] + '\n'
    for f in getSpriteSheets(UNITS[hero_id]):
        s = re.sub(f"({f[5:f.rindex(' ')]} Mini Unit)", f[5:] + '\n\\1', s, 1)
    s += '</gallery>\n'

    return s

def Hero(unit_id):
    return

def Enemy(unit_id):
    unit = UNITS[unit_id]
    name = util.getName(unit_id)

    s = "{{#invoke:NameAbout|main|Name="+util.getName(unit_id,False)+"}}{{HeroPage Tabs}}\n"
    s += EnemyInfobox(unit) + '\n'
    s += UnitStats(unit['base_stats'],unit['growth_rates']).replace('Stats Page','Stats Page/Enemy') + '\n'
    s += UnitSkills([unit['top_weapon'],unit['assist1'],unit['assist2'],unit['_unknown1']], True) + '\n'
    s += "{{Enemies Navbox}}"

    ret = {
        name: s,
        name+'/Quotes': Quotes(unit_id),
        name+'/Misc': Misc(unit_id)
    }

    return ret

def EnemyToHero(hero_id):
    from wikiUtil import getPageContent
    page = getPageContent(util.getName(hero_id))
    return page

def HeroesFrom(tag_ig):
    return

def EnemiesFrom(tag_id):
    enemies = util.readFehData('Common/SRPG/Enemy/'+tag_id+'.json')
    res = {}
    for enemy in enemies:
        res.update(Enemy(enemy['id_tag']))
    return res

def UnitsFrom(tag_id):
    return


from sys import argv
if __name__ == '__main__':
    for arg in argv[1:]:
        try:
            r = {}
            if re.match(r'PID_\w+', arg):
                r = Hero(arg)
            if re.match(r'EID_\w+', arg):
                r = Enemy(arg)
            elif re.match(r'\d+_\w+|v\d{4}[a-e]_\w+', arg):
                r = EnemiesFrom(arg)
            else:
                print('Invalid argument:', arg)
                continue
            for k,v in r.items():
                print(k,v)
        except:
            print('Error with ' + arg)