#! /usr/bin/env python3

from datetime import datetime
from os.path import isfile
from Reverse.RevSD import reverseSummonerDuelsS

import util
from Reverse import reverseSummonerDuelsR
from util import fetchFehDataFromAssets
from reward import parseReward

TAG_TO_EVENT = {
    'ra': 'R',
    'rb': 'S',
}

def SDInfobox(data: dict, common: dict, captainSkills: dict):
    return f"{{{{Summoner Duels {TAG_TO_EVENT[data['id_tag'][:2]]} Infobox\n" + \
         "|mapImage={{MapLayout "+common['fixed_map_id']+"}}\n" + \
         "|bonusHeroes=" + ';'.join([util.getName(u) for u in common['bonus_units']]) + '\n' + \
         "|captainSkills=" + ';'.join([util.getName('MID_REALTIME_PVP_SKILL_'+captainSkills[s]['id_tag']) for s in common['captain_skills']]) + '\n' + \
        f"|startTime={data['avail']['start']}\n" + \
        f"|endTime={util.timeDiff(data['avail']['finish'])}\n" + \
        "}}"

def SDAvailability(data: dict):
    start = datetime.strptime(data['avail']['start'], util.TIME_FORMAT)
    season100 = datetime.strptime("2018-12-25T07:00:00Z", util.TIME_FORMAT)
    s = "==Availability==\nThis [[Summoner Duels "+TAG_TO_EVENT[data['id_tag'][:2]]+"]] event was made available:\n" + \
        f"* {{{{HT|{data['avail']['start']}}}}} — {{{{HT|{util.timeDiff(data['avail']['finish'])}}}}} " + \
        f"([[Summoner Duels {TAG_TO_EVENT[data['id_tag'][:2]]} Has Begun! ({start.strftime('%b %Y').replace(' 0',' ')}) (Notification)|Notification]])" + \
        f"\n** It happened during [[Coliseum season {int((start - season100).days / 7) + 100}]]."
    return s

def SDRewards(data: dict):
    s = "==Rewards==\n===Rank rewards===\n{{#invoke:Reward/EventsSummonerDuels|rank\n"
    maxsize = len(str(data['rank_rewards'][-1]['rank_hi']))
    for rank in data['rank_rewards']:
        ranks = "{{:>{}}}~{{:>{}}}".format(maxsize, maxsize).format(rank['rank_hi'], rank['rank_lo'] if rank['rank_lo'] != -1 else "")
        s += f" |{ranks}=" + parseReward(rank['reward']) + "\n"
    s += "}}\n===Tier rewards===\n{{#invoke:Reward/EventsSummonerDuels|tier\n"
    for r in data['tier_rewards']:
        s += f" |{r['tier']+1}=" + parseReward(r['reward']) + "\n"
    return s + "}}"

def SummonerDuelsR(tag: str):
    SDRdatas = reverseSummonerDuelsR(tag)
    SDdatas = fetchFehDataFromAssets('Common/SRPG/RealTimePvP/Stage/', None)
    captainSkills = fetchFehDataFromAssets('Common/SRPG/RealTimePvP/CaptainSkill/', 'id_num')

    ret = {}
    for SDRdata in SDRdatas:
        SDdata = [d for d in SDdatas if d['avail']['start'] <= SDRdata['avail']['start'] and d['avail']['finish'] >= SDRdata['avail']['finish']][0]
        s = SDInfobox(SDRdata, SDdata, captainSkills) + "\n"
        s += SDAvailability(SDRdata) + "\n"
        s += SDRewards(SDRdata) + "\n"
        s += "==Trivia==\n*\n{{Main Events Navbox}}"
        nb = int(util.cargoQuery('EventsSummonerDuels', 'COUNT(DISTINCT _pageName)=Nb', where=f"_pageName LIKE 'Summoner Duels R %' AND StartTime < '{SDRdata['avail']['start']}'", limit=1)) + 1
        ret[f"Summoner Duels R {nb}"] = s
    return ret

def SummonerDuelsS(tag: str):
    SDSdatas = reverseSummonerDuelsS(tag)
    SDdatas = fetchFehDataFromAssets('Common/SRPG/RealTimePvP/Stage/', None)
    captainSkills = fetchFehDataFromAssets('Common/SRPG/RealTimePvP/CaptainSkill/', 'id_num')

    ret = {}
    for SDSdata in SDSdatas:
        SDdata = [d for d in SDdatas if d['avail']['start'] <= SDSdata['avail']['start'] and d['avail']['finish'] >= SDSdata['avail']['finish']][0]
        s = SDInfobox(SDSdata, SDdata, captainSkills) + "\n"
        s += SDAvailability(SDSdata) + "\n"
        s += SDRewards(SDSdata) + "\n"
        s += "==Trivia==\n*\n{{Main Events Navbox}}"
        nb = int(util.cargoQuery('EventsSummonerDuels', 'COUNT(DISTINCT _pageName)=Nb', where=f"_pageName LIKE 'Summoner Duels S %' AND StartTime < '{SDSdata['avail']['start']}'", limit=1)) + 1
        ret[f"Summoner Duels S {nb}"] = s
    return ret

from sys import argv

if __name__ == '__main__':
    if len(argv) <= 1:
        print("Enter at least one update tag")
        exit(1)
    for arg in argv[1:]:
        if isfile(util.BINLZ_ASSETS_DIR_PATH + 'Common/SRPG/RealTimePvP/Rate/' + arg + '.bin.lz'):
            SDRs = SummonerDuelsR(arg)
            for SDR in SDRs:
                print(SDR, SDRs[SDR])
        if isfile(util.BINLZ_ASSETS_DIR_PATH + 'Common/SRPG/RealTimePvP/AdvancedRate/' + arg + '.bin.lz'):
            SDSs = SummonerDuelsS(arg)
            for SDS in SDSs:
                print(SDS, SDSs[SDS])