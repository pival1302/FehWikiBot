#! /usr/bin/env python3

import re
from os import listdir

from globals import DATA
import util
from Reverse import reverseMjolnirFacility, reverseARCurrency
from wikiUtil import exportSeveralPages, getPageContent, waitSec
from mapUtil import InOtherLanguage

def createStructure(id):
    CATEGORIES = {0:'Structures (D)',4:'Structures (O)',1:'Traps',7:'Decoy Traps',2:'Resources',3:'Ornaments',5:'R&R Structures'}# 0/4 derived from 6
    datas = util.fetchFehData('Common/SkyCastle/FacilityData', easySort=False)
    datas = [data for data in datas if re.sub('\\d*$','',data['id_tag']) == id]
    datas.sort(key=lambda struct: struct['level'])

    params = [str(data['a0']) for data in datas]
    costs = [str(data['cost']) for data in datas]
    currency = [util.getName('MID_ITEM_SKYCASTLE_'+data['cost_type']).replace('Stones','Stone') for data in datas]
    data2 = {curr['id_tag']: curr for f in listdir(util.BINLZ_ASSETS_DIR_PATH + 'Common/SkyCastle/ConsumeItemData/') for curr in reverseARCurrency(f.replace('.bin.lz',''))}[datas[0]['cost_type']]

    s = "{{Structure\n"
    s += '|category=' + CATEGORIES[datas[0]['category']] + '\n'
    s += "|description=" + util.getName('ID_SCF_'+id+'_HELP').replace('\n',' ') + '\n'
    s += '|descriptionResort=' + (DATA['MID_SCF_'+id+'_HOLIDAYHELP'] if 'MID_SCF_'+id+'_HOLIDAYHELP' in DATA else util.getName('ID_SCF_'+id+'_HELP')).replace('\n',' ') + '\n'
    if any([param != '0' for param in params]):
        s += '|parameters0=' + ','.join(params) + '\n'
    s += '|costs=' + ';'.join(costs) + '\n'
    s += '|currency=' + currency[0] + '\n'
    for i,curr in enumerate(currency):
        if curr == currency[0]: continue
        s += f'|currency{i+1}={curr}\n'
    if not re.search('\\d+',datas[0]['id_tag']):
        s += '|nolv=1\n'
    s += '}}\n'
    s += '==Availability==\nThis [[structure]] was made available:\n'
    if not data2['avail']:
        s += '* {{HT|' + util.askFor(intro='When was made available '+util.getName('MID_SCF_' + id)) + '}}'
    else:
        s += '* {{HT|' + data2['avail']['start'] + '}} – {{HT|' + util.timeDiff(data2['avail']['finish']) + '}}'
    s += ' ([[' + util.askFor(intro='What is the introductory notification for '+util.getName('MID_SCF_' + id)) + '|Notification]])\n'
    s += InOtherLanguage('MID_SCF_' + id)
    s += '{{Structures Navbox}}'
    return {util.getName('MID_SCF_' + id): s}

def updateStructure(id):
    datas = util.fetchFehData('Common/SkyCastle/FacilityData', easySort=False)
    datas = [data for data in datas if re.sub('\\d*$','',data['id_tag']) == id]
    datas.sort(key=lambda struct: struct['level'])

    params = [str(data['a0']) for data in datas]
    costs = [str(data['cost']) for data in datas]
    currency = ['Aether Stone' if data['cost_type']=='STONE' else 'Heavenly Dew' if data['cost_type']=='DROP' else ('<!--'+data['cost_type']+'-->') for data in datas]

    page = getPageContent(util.getName('MID_SCF_' + id))[util.getName('MID_SCF_' + id)]
    page = re.sub(r'parameters0\s*=[^\n|}]*','parameters0='+','.join(params),page,1)
    page = re.sub(r'costs\s*=[^\n|}]*','costs='+';'.join(costs),page,1)
    if not re.search(r'\|\s*currency\s*=', page):
        if not re.search(r'\|\s*currency', page):
            page = re.sub(r'(costs\s*=[^\n|}]+)','\\1\n'+'|currency='+currency[0],page,1)
        else:
            page = re.sub(r'(\|\s*currency)','|currency='+currency[0]+'\n\\1',page,1)
    else:
        page = re.sub(r'currency\s*=[^\n|}]+','currency='+currency[0],page,1)
    for (idx, curr) in [(i+1,c) for (i,c) in enumerate(currency) if c != currency[0]]:
        if page.find(f'currency{idx}=') != -1:
            page = re.sub(f'currency{idx}=[^\\n|}}]+',f'currency{idx}={curr}',page,1)
        else:
            page = re.sub(f'(.*currency\\d*=[^\\n|}}]+)',f'\\1\n|currency{idx}={curr}',page,1,re.DOTALL)
    return {util.getName('MID_SCF_' + id): page}

def updateMechanism(id):
    datas = util.fetchFehDataFromAssets('Common/Mjolnir/FacilityData', easySort=False)
    datas = [data for data in datas if re.sub('\\d*','',data['id_tag']) == id]
    datas.sort(key=lambda mech: mech['level'])

    turns = []
    descs = []
    params = []
    costs = []
    for i, data in enumerate(datas):
        turns.append(str(data['turns']))
        params.append(str(data['a0']))
        costs.append(str(data['cost']))
        if 'MID_MF_' + data['id_tag'] + '_HELP' in DATA:
            descs.append(DATA['MID_MF_' + data['id_tag'] + '_HELP'].replace('\n',' '))
        elif 'MID_MF_' + id + '_HELP' in DATA:
            descs.append(DATA['MID_MF_' + id + '_HELP'].replace('\n',' '))
        else:
            descs.append(descs[-1])
    
    if all([desc == descs[0] for desc in descs]):
        desc = re.sub('^\\$a','',descs[0])
    else:
        miss1 = min([min([i for i in range(min(len(descs[0]),len(d))) if descs[0][i] != d[i]]+[min(len(descs[0]),len(d))]) for d in descs])
        miss2 = min([min([i for i in range(min(len(descs[0]),len(d))) if descs[0][-i] != d[-i]]+[min(len(descs[0]),len(d))]) for d in descs])-1
        params = [d[miss1:-miss2] for d in descs]
        desc = descs[0][:miss1] + '$a0' + descs[0][-miss2:]

    page = getPageContent(util.getName('MID_MF_' + id))[util.getName('MID_MF_' + id)]
    page = re.sub(r'description=[^\n|}]+','description='+desc,page)
    page = re.sub(r'parameters0=[^\n|}]+','parameters0='+','.join(params),page)
    page = re.sub(r'turns=[^\n|}]+','turns='+';'.join(turns if any([turn != turns[0] for turn in turns]) else [turns[0]]),page)
    page = re.sub(r'costs=[^\n|}]+','costs='+';'.join(costs),page)
    return {util.getName('MID_MF_' + id): page}

def updateFrom(update_tag):
    structData = util.readFehData('Common/SkyCastle/FacilityData/' + update_tag + '.json')
    mechanData = reverseMjolnirFacility(update_tag) or {}
    ret = {}
    for data in structData:
        tag = re.sub('\\d*$','',data['id_tag'])
        try: ret.update(updateStructure(tag))
        except:
            try:
                ret.update(createStructure(tag))
            except:
                print('Failed to create and update structure ' + util.getName('MID_SCF_'+tag))
    for data in mechanData:
        tag = re.sub('\\d*$','',data['id_tag'])
        try: ret.update(updateMechanism(tag))
        except: print('Failed to update mechanism ' + util.getName('MID_MF_'+tag))
    return ret

from sys import argv
if __name__ == '__main__':
    if len(argv) <= 1:
        print("Enter at least one update tag")
        exit(1)
    for arg in argv[1:]:
        if not re.match(r'\d+_\w+|v\d{4}[a-e]_\w+', arg): continue
        obj = updateFrom(arg)
        for o in obj:
            print(o, obj[o])