#! /usr/bin/env python3

from os.path import isfile
import re

import util
import wikiUtil
from Reverse import reverseHallOfForms
from reward import parseReward

def HoFInfobox(data: dict, nb: int):
    return "{{Hall of Forms Infobox\n" + \
        f"|number={nb}\n" + \
        f"|promoArt=Hall of Forms {nb}.jpg\n" + \
        f"|forma={';'.join([util.getName(unit) for unit in data['forma']['heroes']])}\n" + \
        f"|startTime={data['avail']['start']}\n" + \
        f"|endTime={util.timeDiff(data['avail']['finish'])}\n}}}}"

def HoFRewards(data: dict):
    s = "==Rewards==\n===Daily rewards===\n{{#invoke:Reward/HallOfForms|daily\n"
    for i in range(len(data['daily_bonuses'])):
        s += f" |{i+1}=" + parseReward(data['daily_bonuses'][i]['reward']) + "\n"
    s += "}}\n===Chamber cleared===\n{{#invoke:Reward/HallOfForms|chambers\n"
    for chamber in data['chambers']:
        s += f" | {util.getName('MID_IDOL_TOWER_STAGE_'+chamber['id_tag'])} = " + parseReward(chamber['reward']) + "\n"
    return s + "}}"

def HallOfFormsOriginal(data):
    if data["is_revival"]:
        raise ValueError("Given Hall of Forms is a revival")

    nb = int(util.cargoQuery('HallOfForms', 'COUNT(DISTINCT _pageName)=Nb', where=f"StartTime < '{data['avail']['start']}'", limit=1)) + 1
    s = HoFInfobox(data, nb) + "\n"
    s += HoFRewards(data) + "\n"
    s += "{{Main Events Navbox}}"
    return {f"Hall of Forms {nb}": s}

def HallOfFormsRevival(data):
    if not data['is_revival']:
        raise ValueError("Given Hall of Forms is not a revival")

    query = util.cargoQuery('HallOfForms', '_pageName=Page,StartTime', ' AND '.join(["Forma HOLDS '"+util.getName(h).replace("'","\\'")+"'" for h in data['forma']['heroes']]), limit=1)
    if not query:
        raise ValueError("Fail to retrieve original Hall of Forms")
    name = query['Page']
    page = wikiUtil.getPageContent(name)[name]

    origStart = query['StartTime'].replace(' ','T') + 'Z'
    start = data["avail"]["start"]
    end = util.timeDiff(data["avail"]["finish"])
    rewards = HoFRewards(data)

    if page.find(start) == -1:
        page = re.sub(r"(\|startTime=[^\n|}]*)\n", f"\\1,{start}\n", page, 1)
    if page.find(end) == -1:
        page = re.sub(r"(\|endTime=[^\n|}]*)\n", f"\\1,{end}\n", page, 1)
    
    if not re.search(r"===\s*Daily rewards\s*===\n\{\{Tab/start\}\}", page):
        page = re.sub("(===\s*Daily rewards\s*===)(.+?)(?=\n+==)", "\\1\n{{Tab/start}}\n{{Tab/header|Original}}\\2\n{{Tab/end}}", page, flags=re.DOTALL)
    if not re.search(r'#invoke:Reward/HallOfForms\|daily\s*\|startTime', page):
        page = page.replace('#invoke:Reward/HallOfForms|daily','#invoke:Reward/HallOfForms|daily|startTime='+origStart)
    if not re.search(r'daily\s*\|startTime='+start, page):
        reward1 = rewards[rewards.find('{{'):rewards.find('}}')+2].replace('|daily','|daily|startTime='+start)
        page = re.sub(r'(#invoke:Reward/HallOfForms\|daily.*?)\n(\{\{Tab/end\}\})', '\\1\n{{Tab/header|Revival}}\n'+reward1+'\n\\2', page, flags=re.DOTALL)
    
    if not re.search(r"===\s*Chamber cleared\s*===\n\{\{Tab/start\}\}", page):
        page = re.sub("(===\s*Chamber cleared\s*===)(.+?)(?=\n+==)", "\\1\n{{Tab/start}}\n{{Tab/header|Original}}\\2\n{{Tab/end}}", page, flags=re.DOTALL)
    if not re.search(r'#invoke:Reward/HallOfForms\|chambers\s*\|startTime', page):
        page = page.replace('#invoke:Reward/HallOfForms|chambers','#invoke:Reward/HallOfForms|chambers|startTime='+origStart)
    if not re.search(r'chambers\s*\|startTime='+start, page):
        reward1 = rewards[rewards.rfind('{{'):rewards.rfind('}}')+2].replace('|chambers','|chambers|startTime='+start)
        page = re.sub(r'(#invoke:Reward/HallOfForms\|chambers.*?)\n(\{\{Tab/end\}\})', '\\1\n{{Tab/header|Revival}}\n'+reward1+'\n\\2', page, flags=re.DOTALL)
    
    return {name: page}

def HallOfForms(tag: str):
    datas = reverseHallOfForms(tag)
    content = {}
    for data in datas:
        if data["is_revival"]:
            content.update(HallOfFormsRevival(data))
        else:
            content.update(HallOfFormsOriginal(data))
    return content

def exportHallOfForms(tagId: str):
    content = HallOfForms(tagId)
    for name in content:
        isOriginal = True if content[name].find("{{Tab/header|Revival}}",) == -1 else False
        wikiUtil.exportPage(name, content[name], 'Bot: Hall of Forms' if isOriginal else 'Bot: Hall of Forms revival', create=isOriginal)
    return content


from sys import argv

if __name__ == '__main__':
    if len(argv) <= 1:
        print("Enter at least one update tag")
        exit(1)
    for arg in argv[1:]:
        if not isfile(util.BINLZ_ASSETS_DIR_PATH + 'Common/SRPG/IdolTower/' + arg + '.bin.lz'):
            print(f'No Hall of Forms are related to the tag "{arg}"')
            continue
        hofs = HallOfForms(arg)
        for hof in hofs:
            print(hof, hofs[hof])